# TemplateId - EHRN Adverse reactions.v0
This composition is intended to be a persistent curated list of a person's allergies & adverse reactions. It has significance across a patient's encounters and is curated from information gathered across clinical events. While it is possible to build a solution to curate this automatically, in realife it is often managed by the care team members.
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Substance | Text | Intolerance to wheat  | Code using SNOMED CT? |
| 2 | Status | coded_text | Suspected | Archetype set - Suspected (at0127); Likely (at0064); Confirmed (at0065); Resolved (at0067); Refuted (at0066) | 
| 3 | Category | coded_text | Food | Archetype set - Food (at0121); Medication (at0122); Other (at0123) |
| 4 | Criticality | coded_text | Low | Archetype set - Low (at0102); High (at0103); Indeterminate (at0124) |
| 5 | Comment | Text | Severe indigestion and bloating | |

## Sample composition
- templateId : EHRN Adverse reactions.v0
- format : FLAT

```Compact json
{
"ctx/language": "en",
"ctx/territory": "IN",
"ctx/time": "2022-04-18T12:40:31.418954+02:00",
"ctx/id_namespace": "EHR.NETWORK",
"ctx/id_scheme": "UUID",
"ctx/composer_name": "Silvia Blake",
"ctx/composer_id": "123",
"ctx/health_care_facility|name": "Hospital",
"ctx/health_care_facility|id": "9091",
"ctx/provider_name": "Silvia Blake",
"ctx/provider_id": "123",
"ctx/location": "Lab B2",
"ctx/setting": "secondary medical care",
"adverse_reaction_list/adverse_reaction_risk:0/substance|value": "Intolerance to wheat",
"adverse_reaction_list/adverse_reaction_risk:0/substance|code": "700095006",
"adverse_reaction_list/adverse_reaction_risk:0/substance|terminology": "SNOMED CT",
"adverse_reaction_list/adverse_reaction_risk:0/status|code": "at0127",
"adverse_reaction_list/adverse_reaction_risk:0/status|value": "Suspected",
"adverse_reaction_list/adverse_reaction_risk:0/status|terminology": "local",
"adverse_reaction_list/adverse_reaction_risk:0/category|code": "at0121",
"adverse_reaction_list/adverse_reaction_risk:0/category|value": "Food",
"adverse_reaction_list/adverse_reaction_risk:0/category|terminology": "local",
"adverse_reaction_list/adverse_reaction_risk:0/criticality|code": "at0102",
"adverse_reaction_list/adverse_reaction_risk:0/criticality|value": "Low",
"adverse_reaction_list/adverse_reaction_risk:0/criticality|terminology": "local",
"adverse_reaction_list/adverse_reaction_risk:0/comment": "Severe indigestion and bloating"
 }

```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 


