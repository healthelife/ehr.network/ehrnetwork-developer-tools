# TemplateId - EHRN Family history.v1
Records the history of significant health related issues in related family members of the patient. It has significance across a patient's encounters.
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Relationship | Text | Father  | Code using SNOMED CT? |
| 2 | Problem/Diagnosis name | Text | Diabetes mellitus | Code using SNOMED CT? | 
| 3 | Clinical description | Text | Description of family history | |
| 4 | Age at onset | Duration | 52 Years | PT0S format |

## Sample composition
- templateId : EHRN Family history.v1
- format : FLAT

```Compact json
{
"ctx/language": "en",
"ctx/territory": "IN",
"ctx/time": "2022-04-18T12:40:31.418954+02:00",
"ctx/id_namespace": "EHR.NETWORK",
"ctx/id_scheme": "UUID",
"ctx/composer_name": "Silvia Blake",
"ctx/composer_id": "123",
"ctx/health_care_facility|name": "Hospital",
"ctx/health_care_facility|id": "9091",
"ctx/provider_name": "Silvia Blake",
"ctx/provider_id": "123",
"ctx/location": "Lab B2",
"ctx/setting": "secondary medical care",
"family_history/family_history:0/per_family_member:0/relationship|value": "Father",
"family_history/family_history:0/per_family_member:0/relationship|code": "66839005",
"family_history/family_history:0/per_family_member:0/relationship|terminology": "SNOMED CT",
"family_history/family_history:0/per_family_member:0/clinical_history:0/problem_diagnosis_name|value": "Diabetes mellitus",
"family_history/family_history:0/per_family_member:0/clinical_history:0/problem_diagnosis_name|code": "73211009",
"family_history/family_history:0/per_family_member:0/clinical_history:0/problem_diagnosis_name|terminology": "SNOMED CT",
"family_history/family_history:0/per_family_member:0/clinical_history:0/clinical_description": "Description of family history 2",  
"family_history/family_history:0/per_family_member:0/clinical_history:0/age_at_onset": "P52YT0S"
 }

```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

