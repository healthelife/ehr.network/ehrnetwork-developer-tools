# TemplateId - EHRN Clinical notes.v1
Can be used to general unstructured clinical notes.
## Data points 
This template alllows recording one a simgle desciptive clinical note. 

| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Synopsis | Text | Severe indigestion and bloating | |

## Sample composition
- templateId : EHRN Clinical notes.v1
- format : FLAT

```Compact json
{
"ctx/language": "en",
"ctx/territory": "IN",
"ctx/time": "2022-04-18T12:40:31.418954+02:00",
"ctx/id_namespace": "EHR.NETWORK",
"ctx/id_scheme": "UUID",
"ctx/composer_name": "Silvia Blake",
"ctx/composer_id": "123",
"ctx/health_care_facility|name": "Hospital",
"ctx/health_care_facility|id": "9091",
"ctx/provider_name": "Silvia Blake",
"ctx/provider_id": "123",
"ctx/location": "Lab B2",
"ctx/setting": "secondary medical care",
"clinical_notes/clinical_synopsis:0/synopsis": "Clinical notes"
 }

```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

