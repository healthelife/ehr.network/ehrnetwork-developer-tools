# TemplateId - EHRN Vital signs.v1
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample |
|---|-----|-----------|---------|
| 1 | Systolic BP | Qty | 120 mm[HG] |
| 2 | Diastolic BP | Qty | 80 mm[HG] |
| 3 | Pulse rate | Qty | 72 /min |
| 4 | Temperature | Qty | 35 Cel |
| 5 | Respiration rate | Qty | 25 /min |
| 6 | SPo2 | % | 97 |
| 7 | Clinical synopsis | Text |  |

## Sample composition

- templateId : EHRN Vital signs.v1
- format : FLAT

```FLAT json - compact
Can be used to create and update compositions. 

{
"ctx/language": "en",
"ctx/territory": "IN",
"ctx/time": "2022-04-18T12:40:31.418954+02:00",
"ctx/id_namespace": "EHR.NETWORK",
"ctx/id_scheme": "UUID",
"ctx/composer_name": "Silvia Blake",
"ctx/composer_id": "123",
"ctx/health_care_facility|name": "Hospital",
"ctx/health_care_facility|id": "9091",
"ctx/provider_name": "Silvia Blake",
"ctx/provider_id": "123",
"ctx/location": "Lab B2",
"ctx/setting": "secondary medical care",
"vital_signs/vital_signs:0/blood_pressure:0/any_event:0/systolic|magnitude": 120.0,
"vital_signs/vital_signs:0/blood_pressure:0/any_event:0/systolic|unit": "mm[Hg]",
"vital_signs/vital_signs:0/blood_pressure:0/any_event:0/diastolic|magnitude": 80.0,
"vital_signs/vital_signs:0/blood_pressure:0/any_event:0/diastolic|unit": "mm[Hg]",
"vital_signs/vital_signs:0/body_temperature:0/any_event:0/temperature|magnitude": 35.0,
"vital_signs/vital_signs:0/body_temperature:0/any_event:0/temperature|unit": "Cel",
"vital_signs/vital_signs:0/respirations:0/any_event:0/rate|magnitude": 25.0,
"vital_signs/vital_signs:0/respirations:0/any_event:0/rate|unit": "/min",
"vital_signs/vital_signs:0/pulse_oximetry:0/any_event:0/spo|numerator": 97,
"vital_signs/vital_signs:0/pulse_oximetry:0/any_event:0/spo|denominator": 100,
"vital_signs/vital_signs:0/pulse_oximetry:0/any_event:0/spo|type": 2,
"vital_signs/vital_signs:0/pulse_heart_beat:0/any_event:0/pulse_rate|magnitude": 72.0,
"vital_signs/vital_signs:0/pulse_heart_beat:0/any_event:0/pulse_rate|unit": "/min",
"vital_signs/vital_signs:0/clinical_synopsis/synopsis": "All vitals normal"
}

```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 


