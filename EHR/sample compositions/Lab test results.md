# TemplateId - EHRN Lab test results.v0
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Test name | Text | Complete blood count  | Code using SNOMED CT/LOINC? |
| 2 | Overall test status timestamp | Date | | | 
| 3 | Analyte name 1 | Text | RBC | Code using SNOMED CT/LOINC? |
| 4 | Analyte result 1| Quantity | 4.1 million/cmm | |
| 5 | Analyte name 2 | Text | Lymphocyte | Code using SNOMED CT/LOINC? |
| 6 | Analyte result 2 | Quantity | 21 % | |
| 7 | Conclusion | Text | All Values in range | |

## Sample composition
- templateId : EHRN Lab test results.v0
- format : FLAT 

```json
{
"ctx/language": "en",
"lab_test_results/territory|code": "IN",
"lab_test_results/territory|terminology": "ISO_3166-1",
"lab_test_results/language|code": "en",
"lab_test_results/language|terminology": "ISO_639-1",
"lab_test_results/composer|id": "1234-5678", 
"lab_test_results/composer|id_scheme": "UUID", 
"lab_test_results/composer|id_namespace": "EHR.NETWORK", 
"lab_test_results/composer|name": "Dileep",
"lab_test_results/context/health_care_facility|id": "123456-123",
"lab_test_results/context/health_care_facility|id_scheme": "UUID",
"lab_test_results/context/health_care_facility|id_namespace": "EHR.NETWORK",
"lab_test_results/context/health_care_facility|name": "HealtheLife",
"lab_test_results/context/setting|code": "232",
"lab_test_results/context/setting|value": "secondary medical care",
"lab_test_results/context/setting|terminology": "openehr",
"lab_test_results/context/ehrn_metadata:0/confidentiality_level": "5",  
"lab_test_results/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"lab_test_results/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
"lab_test_results/context/ehrn_metadata:0/program_details/application_name": "Resort",
"lab_test_results/context/ehrn_metadata:0/program_details/application_id": "234567-89",
"lab_test_results/laboratory_test_result:0/any_event:0/test_name|value": "Complete blood count",
"lab_test_results/laboratory_test_result:0/any_event:0/test_name|code": "26604007",
"lab_test_results/laboratory_test_result:0/any_event:0/test_name|terminology": "SNOMED CT",  
"lab_test_results/laboratory_test_result:0/any_event:0/overall_test_status_timestamp": "2020-01-10T00:00:00.000Z", 
"lab_test_results/laboratory_test_result:0/any_event:0/laboratory_analyte_result:0/analyte_name|value": "RBC",
"lab_test_results/laboratory_test_result:0/any_event:0/laboratory_analyte_result:0/analyte_name|code": "41898006",
"lab_test_results/laboratory_test_result:0/any_event:0/laboratory_analyte_result:0/analyte_name|terminology": "SNOMED CT",  
"lab_test_results/laboratory_test_result:0/any_event:0/laboratory_analyte_result:0/analyte_result:0/quantity_value|magnitude": "4.1",
"lab_test_results/laboratory_test_result:0/any_event:0/laboratory_analyte_result:0/analyte_result:0/quantity_value|unit": "million/cmm",
"lab_test_results/laboratory_test_result:0/any_event:0/laboratory_analyte_result:1/analyte_name|value": "Lymphocyte",
"lab_test_results/laboratory_test_result:0/any_event:0/laboratory_analyte_result:1/analyte_name|code": "56972008",
"lab_test_results/laboratory_test_result:0/any_event:0/laboratory_analyte_result:1/analyte_name|terminology": "SNOMED CT",  
"lab_test_results/laboratory_test_result:0/any_event:0/laboratory_analyte_result:1/analyte_result:0/quantity_value|magnitude": "21",
"lab_test_results/laboratory_test_result:0/any_event:0/laboratory_analyte_result:1/analyte_result:0/quantity_value|unit": "%",
"lab_test_results/laboratory_test_result:0/any_event:0/conclusion": "All Values in range"
}


```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Lab test results.v0
- personId : 

#### Body : 
```json
{

    "name": "lab_test_results_aql"
    }


```
or
```json

{

    "name": "lab_test_results_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
}
```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as createdTime, a/uid/value as uid,b/data[at0001]/events[at0002]/data[at0003]/items[at0005]/value as testName, b/data[at0001]/events[at0002]/data[at0003]/items[at0005]/value/defining_code/code_string as testCode, b/data[at0001]/events[at0002]/data[at0003]/items[at0005]/value/defining_code/terminology_id/value as testCodeTerminology,b/data[at0001]/events[at0002]/data[at0003]/items[at0075]/value as resultTime,c/items[at0024]/value as analyte,c/items[at0024]/value/defining_code/code_string as analyteCode, c/items[at0024]/value/defining_code/terminology_id/value as analyteCodeTerminology, c/items[at0001]/value/magnitude as resultValue,c/items[at0001]/value/units as resultUnit,c/items[at0004]/value as referenceRange,a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value ='{{ehrId}}'] CONTAINS COMPOSITION a CONTAINS OBSERVATION b[openEHR-EHR-OBSERVATION.laboratory_test_result.v1] CONTAINS CLUSTER c[openEHR-EHR-CLUSTER.laboratory_test_analyte.v1]"
}
```
