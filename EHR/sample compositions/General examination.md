# TemplateId - EHRN General examination.v1
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Time | DateTime | | |
| 2 |Build | Text | Build |  |
| 3 | Fatigue | Text | Fatigue |  |
| 4 | Temperature | Text | Temperature | |
| 5 |Pallor | Text | Pallor | |
| 6 | Icterus | Text | Icterus | |
| 7 | Cyanosis | Text | Cyanosis | |
| 8 | Clubbing | Text | Clubbing | |
| 9 | Lymphadenopathy | Text | cLymphadenopathy | |
| 10 | Edema | Text | Edema | |
| 11 | Interpretation | Text | Some interpretation 2 | |

## Sample composition
- templateId : EHRN General examination.v1
- format : FLAT

```json
{
"ctx/language": "en",
"general_examination/territory|code": "IN",
"general_examination/territory|terminology": "ISO_3166-1",
"general_examination/language|code": "en",
"general_examination/language|terminology": "ISO_639-1",
"general_examination/composer|id": "1234-5678", 
"general_examination/composer|id_scheme": "UUID", 
"general_examination/composer|id_namespace": "EHR.NETWORK", 
"general_examination/composer|name": "Dileep",
"general_examination/context/health_care_facility|id": "123456-123",
"general_examination/context/health_care_facility|id_scheme": "UUID",
"general_examination/context/health_care_facility|id_namespace": "EHR.NETWORK",
"general_examination/context/health_care_facility|name": "HealtheLife",
"general_examination/context/setting|code": "232",
"general_examination/context/setting|value": "secondary medical care",
"general_examination/context/setting|terminology": "openehr",
"general_examination/context/ehrn_metadata:0/confidentiality_level": "5",
"general_examination/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"general_examination/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
"general_examination/context/ehrn_metadata:0/program_details/application_name": "Resort",
"general_examination/context/ehrn_metadata:0/program_details/application_id": "234567-89",
"general_examination/physical_examination_findings:0/any_event:0/time": "2019-05-11T00:00:00.000Z",
"general_examination/physical_examination_findings:0/any_event:0/general_examination:0/build": "Build",  
"general_examination/physical_examination_findings:0/any_event:0/general_examination:0/fatigue": "Fatigue",  
"general_examination/physical_examination_findings:0/any_event:0/general_examination:0/temperature": "Temperature",  
"general_examination/physical_examination_findings:0/any_event:0/general_examination:0/pallor": "Pallor",  
"general_examination/physical_examination_findings:0/any_event:0/general_examination:0/icterus": "Icterus",  
"general_examination/physical_examination_findings:0/any_event:0/general_examination:0/cyanosis": "Cyanosis",  
"general_examination/physical_examination_findings:0/any_event:0/general_examination:0/clubbing": "Clubbing",  
"general_examination/physical_examination_findings:0/any_event:0/general_examination:0/lymphadenopathy": "Lymphadenopathy",  
"general_examination/physical_examination_findings:0/any_event:0/general_examination:0/edema": "Edema",  
"general_examination/physical_examination_findings:0/any_event:0/interpretation:0": "Some interpretation 2"
}






```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN General examination.v1
- personId : 

#### Body : 
```json

{

    "name": "general_examination_aql"
    }


```
or
```json

{

    "name": "general_examination_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    }

```

### AQL

``` json
{"aql":"select  a/composer/name as author,  a/uid/value as uid, a/context/start_time/value as date_created,  b/data[at0001]/events[at0002]/data[at0003]/items[at0006]/value/value as interpretation,  c/items[at0001]/value/value as build,  c/items[at0002]/value/value as fatigue,  c/items[at0003]/value/value as temperature,  c/items[at0004]/value/value as pallor,  c/items[at0005]/value/value as icterus,  c/items[at0006]/value/value as cyanosis,  c/items[at0007]/value/value as clubbing,  c/items[at0008]/value/value as lymphadenopathy,  c/items[at0009]/value as edema,  b/data[at0001]/events[at0002]/time/value as observedTime,  a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel  FROM EHR e[ehr_id/value ='{{ehrId}}' ] CONTAINS COMPOSITION a[openEHR-EHR-COMPOSITION.encounter.v1]  CONTAINS OBSERVATION b[openEHR-EHR-OBSERVATION.exam.v1]  CONTAINS CLUSTER c[openEHR-EHR-CLUSTER.general_examination.v0]"
}
```
