# TemplateId - EHRN Diagnosis.v1
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Problem/Diagnosis name | Text | Chronic schizophrenia  | Code using SNOMED CT? |
| 2 | Severity | coded_text | Mild | Archetype set - Mild(at0047); Moderate(at0048); Severe(at0049) | 
| 3 | Diagnostic certainty | Coded text | Suspected | Archetype set - Suspected(at0074); Probable(at0075); Confirmed(at0076); |
| 4 | Diagnostic category | Coded text | Principal diagnosis | Archetype set - Principal diagnosis(at0064); Secondary diagnosis(at0066); Complication(at0076) |
| 5 | Date/time of onset | Date | | |
| 6 | Resolution phase | Coded text | Resolving | Archetype set - Not resolving(at0086); Resolving(at0085); Resolved(at0084); Indeterminate(at0087)|
| 7 | Clinical description | Text | Physical violence since 6 days | |
## Sample composition
- templateId : EHRN Diagnosis.v1
- format : FLAT

```json
{
"ctx/language": "en",
"diagnosis/territory|code": "IN",
"diagnosis/territory|terminology": "ISO_3166-1",
"diagnosis/language|code": "en",
"diagnosis/language|terminology": "ISO_639-1",
"diagnosis/composer|id": "1234-5678", 
"diagnosis/composer|id_scheme": "UUID", 
"diagnosis/composer|id_namespace": "EHR.NETWORK", 
"diagnosis/composer|name": "Dileep",
"diagnosis/context/health_care_facility|id": "123456-123",
"diagnosis/context/health_care_facility|id_scheme": "UUID",
"diagnosis/context/health_care_facility|id_namespace": "EHR.NETWORK",
"diagnosis/context/health_care_facility|name": "HealtheLife",
"diagnosis/context/setting|code": "232",
"diagnosis/context/setting|value": "secondary medical care",
"diagnosis/context/setting|terminology": "openehr",
"diagnosis/context/ehrn_metadata:0/confidentiality_level": "5",
"diagnosis/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"diagnosis/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
"diagnosis/context/ehrn_metadata:0/program_details/application_name": "Resort",
"diagnosis/context/ehrn_metadata:0/program_details/application_id": "234567-89",
"diagnosis/problem_diagnosis:0/problem_diagnosis_name|value": "Chronic schizophrenia",
"diagnosis/problem_diagnosis:0/problem_diagnosis_name|code": "83746006",
"diagnosis/problem_diagnosis:0/problem_diagnosis_name|terminology": "SNOMED CT", 
"diagnosis/problem_diagnosis:0/severity|code": "at0047",  
"diagnosis/problem_diagnosis:0/severity|value": "Mild",  
"diagnosis/problem_diagnosis:0/severity|terminology": "local", 
"diagnosis/problem_diagnosis:0/diagnostic_certainty|code": "at0074",
"diagnosis/problem_diagnosis:0/diagnostic_certainty|value": "Suspected",
"diagnosis/problem_diagnosis:0/diagnostic_certainty|terminology": "local", 
"diagnosis/problem_diagnosis:0/problem_diagnosis_qualifier:0/diagnostic_category:0|code": "at0064",  
"diagnosis/problem_diagnosis:0/problem_diagnosis_qualifier:0/diagnostic_category:0|value": "Principal diagnosis",  
"diagnosis/problem_diagnosis:0/problem_diagnosis_qualifier:0/diagnostic_category:0|terminology": "local",
"diagnosis/problem_diagnosis:0/date_time_of_onset": "2019-05-30T00:00:00.000Z",
"diagnosis/problem_diagnosis:0/problem_diagnosis_qualifier:0/resolution_phase|code": "at0085",
  "diagnosis/problem_diagnosis:0/problem_diagnosis_qualifier:0/resolution_phase|value": "Resolving",  
  "diagnosis/problem_diagnosis:0/problem_diagnosis_qualifier:0/resolution_phase|terminology": "local",
"diagnosis/problem_diagnosis:0/clinical_description": "Physical violence since 6 days"
}
```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Diagnosis.v1
- personId : 

#### Body : 
```json
{

    "name": "diagnosis_aql"
    }

```
or
```json
{

    "name": "diagnosis_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }

}
```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as createdTime, b/data[at0001]/items[at0002]/value as diagnosis, b/data[at0001]/items[at0002]/value/defining_code/code_string as diagnosisCode, b/data[at0001]/items[at0002]/value/defining_code/terminology_id/value as diagnosisTerminology, b/data[at0001]/items[at0005]/value as severity, b/data[at0001]/items[at0005]/value/defining_code/code_string as severityCode, b/data[at0001]/items[at0005]/value/defining_code/terminology_id/value as severityTerminology, b/data[at0001]/items[at0009]/value as description, c/items[at0004]/value as status, c/items[at0004]/value/defining_code/code_string as statusCode, c/items[at0004]/value/defining_code/terminology_id/value as statusTerminology, c/items[at0063]/value as diagnosticCategory, c/items[at0063]/value/defining_code/code_string as diagnosticCategoryCode, c/items[at0063]/value/defining_code/terminology_id/value as diagnosticCategoryTerminology, a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value ='{{ehrId}}' ] CONTAINS COMPOSITION a[openEHR-EHR-COMPOSITION.encounter.v1] CONTAINS EVALUATION b[openEHR-EHR-EVALUATION.problem_diagnosis.v1] CONTAINS CLUSTER c[openEHR-EHR-CLUSTER.problem_qualifier.v1]"
}
```
