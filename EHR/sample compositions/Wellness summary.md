# TemplateId - EHRN Wellness summary.v2
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Details |
|---|-----|-----------|---------|---------|
| | General wellness |-----------|---------|---------|
| 1 | Feeling of purpose | Coded text | Completely | Local set - Completely; Somewhat; Normal; Purposeless  |
| 2 | Quality of spiritual life | Coded text | Fully satisfied | Local set - Fully satisfied; Somewhat satisfying; No impact; Empty feeling |
| 3 | Climate impact > Cold | Coded text | Comfortable | Local set - Comfortable; Uncomfortable; Depressed |
| 4 | Climate impact > Hot | Coded text | Comfortable |  Local set - Comfortable; Uncomfortable; Depressed |
| 5 | Climate impact > Cool and damp | Coded text | Comfortable | Local set - Comfortable; Uncomfortable; Depressed |
| 6 | Climate impact > Hot and humid | Coded text | Comfortable | Local set - Comfortable; Uncomfortable; Depressed |
| | Social wellness |-----------|---------|---------|
| 1 | Travel often? | Boolean | Completely | Local set - Completely; Somewhat; Normal; Purposeless  |
| 2 | Quality of family relationship | Coded text | Excellent | Local set - Excellent; Good; Fair; Poor |
| 3 | Quality of social life | Coded text | Excellent | Local set - Excellent; Good; Fair; Poor |
| 4 | Ability to express yourself | Coded text | Excellent |  Local set - Excellent; Good; Fair; Poor |
| 5 | Quality of professional career | Coded text | Love it | Local set - Love it; Like it; Bearable; Unbearable |
| | Mental stress |-----------|---------|---------|
| 1 | Stress levels| Text | Low | Local set - Low, Normal, High  |
| 2 | Triggers>Personal | Boolean | false | |
| 3 | Triggers>Family | Boolean | false | |
| 4 | Triggers>Social | Boolean | false | |
| 5 | Triggers>Professional | Boolean | true | |
| 6 | Triggers>Others | Boolean | true| |
| | Sleep |-----------|---------|---------|
| 1 | Sleeping time | Coded text | 7-9 pm | Local set - 7-9 pm; 9-11 pm; 11-12 pm; After 12 pm |
| 2 | Waking time | Coded text | 4-6 am | Archetype set - 4-6 am (at0008); 6-8 am (at0009); 8-10 am (at0010); After 10 am (at0011) |
| 3 | Sleep experience | Coded text | Sound | Local set - Sound; Light & interrupted; Not enough; Heavy and long; Difficulty sleeping; Difficulty waking; Frequent nightmares; Wake up too early |
| 4 | Feeling on wake up | Coded text | Fresh & rested | Local set - Fresh & rested; Little tired; Tired; Very tired |
| 5 | Sleep position | Coded text | On back | Local set - On back; On stomach; Left side; Right side |
| | Bowel movements |-----------|---------|---------|
| 1 | Frequency | Coded text | Every 2-3 days | Archetype set - Every 2-3 days (at0016); Daily; 2-3 times per day (at0017) |
| 2 | Timing | Coded text | Early morning | Local set - Early morning; Late in the day; After meals; After dinner; Need laxative|
| 3 | Nature of stool | Text | Normal consistency | |
| 4 | Associated problems>Pain | Boolean | true | |
| 5 | Associated problems>Blood | Boolean | false | |
| 6 | Associated problems>Mucous | Boolean | false | |
| 7 | Associated problems>Foul smell | Boolean | false | |

## Sample composition

- templateId : EHRN Wellness summary.v2
- format : FLAT

```json
{
"ctx/language": "en",
"wellness_summary/territory|code": "IN",
"wellness_summary/territory|terminology": "ISO_3166-1",
"wellness_summary/language|code": "en",
"wellness_summary/language|terminology": "ISO_639-1",
"wellness_summary/composer|id": "1234-5678", 
"wellness_summary/composer|id_scheme": "UUID", 
"wellness_summary/composer|id_namespace": "EHR.NETWORK", 
"wellness_summary/composer|name": "Dileep",
"wellness_summary/context/health_care_facility|id": "123456-123",
"wellness_summary/context/health_care_facility|id_scheme": "UUID",
"wellness_summary/context/health_care_facility|id_namespace": "EHR.NETWORK",
"wellness_summary/context/health_care_facility|name": "HealtheLife",
"wellness_summary/context/setting|code": "232",
"wellness_summary/context/setting|value": "secondary medical care",
"wellness_summary/context/setting|terminology": "openehr",
"wellness_summary/context/ehrn_metadata:0/confidentiality_level": "5",
"wellness_summary/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"wellness_summary/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
"wellness_summary/context/ehrn_metadata:0/program_details/application_name": "Clinic",
"wellness_summary/context/ehrn_metadata:0/program_details/application_id": "234567-89",
"wellness_summary/wellness_summary:0/general_wellness:0/feeling_of_purpose": "Completely",  
"wellness_summary/wellness_summary:0/general_wellness:0/quality_of_spirtual_life": "Fully satisfied",
"wellness_summary/wellness_summary:0/general_wellness:0/energy_level": "Very high",
"wellness_summary/wellness_summary:0/general_wellness:0/climate_impact/cold": "Comfortable",
"wellness_summary/wellness_summary:0/general_wellness:0/climate_impact/hot": "Comfortable",  
"wellness_summary/wellness_summary:0/general_wellness:0/climate_impact/cool_and_damp": "Comfortable",  
"wellness_summary/wellness_summary:0/general_wellness:0/climate_impact/hot_and_humid": "Comfortable",
"wellness_summary/wellness_summary:0/social_wellness/travel_often": true,  
"wellness_summary/wellness_summary:0/social_wellness/quality_of_family_relationships": "Excellent",
"wellness_summary/wellness_summary:0/social_wellness/quality_of_social_life": "Excellent",  
"wellness_summary/wellness_summary:0/social_wellness/ability_to_express_yourself": "Excellent",  
"wellness_summary/wellness_summary:0/social_wellness/quality_of_professional_career": "Love it",
"wellness_summary/wellness_summary:0/mental_stress:0/stress_level": "High",
"wellness_summary/wellness_summary:0/mental_stress:0/triggers/personal": false,
"wellness_summary/wellness_summary:0/mental_stress:0/triggers/family": false,
"wellness_summary/wellness_summary:0/mental_stress:0/triggers/social": false,
"wellness_summary/wellness_summary:0/mental_stress:0/triggers/professional": true,
"wellness_summary/wellness_summary:0/mental_stress:0/triggers/others": true,
"wellness_summary/wellness_summary:0/sleep:0/sleeping_time": "9-11 pm",
"wellness_summary/wellness_summary:0/sleep:0/waking_time": "6-8 am",
"wellness_summary/wellness_summary:0/sleep:0/sleep_experience": "Sound",
"wellness_summary/wellness_summary:0/sleep:0/feeling_on_wake_up": "Little tired",
"wellness_summary/wellness_summary:0/sleep:0/sleep_position": "On back",
"wellness_summary/wellness_summary:0/bowel_movements:0/frequency": "Daily",
"wellness_summary/wellness_summary:0/bowel_movements:0/timing": "Early morning",
"wellness_summary/wellness_summary:0/bowel_movements:0/nature_of_stool": "Normal consistency",
"wellness_summary/wellness_summary:0/bowel_movements:0/associated_problems/pain": false,
"wellness_summary/wellness_summary:0/bowel_movements:0/associated_problems/blood": true,
"wellness_summary/wellness_summary:0/bowel_movements:0/associated_problems/mucous": true,
"wellness_summary/wellness_summary:0/bowel_movements:0/associated_problems/foul_smell": true
}




```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Wellness summary.v2
- personId : 

#### Body : 
```json
{

    "name": "wellness_summary_aql"
    }

```
or
```json

{

    "name": "wellness_summary_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    }
```

### AQL

``` json
{"aql":"select a/composer/name as author,a/context/start_time/value as createdTime,c/data[at0001]/items[at0002]/value as feelingOfPurpose,c/data[at0001]/items[at0003]/value as qualityOfSpirtualLife,c/data[at0001]/items[at0004]/value as energyLevel,c/data[at0001]/items[at0005]/items[at0006]/value as climateCold,c/data[at0001]/items[at0005]/items[at0007]/value as climateHot,c/data[at0001]/items[at0005]/items[at0008]/value as climateCoolDamp,c/data[at0001]/items[at0005]/items[at0009]/value as climateHotHumid,f/data[at0001]/items[at0002]/value as stressLevel,f/data[at0001]/items[at0004]/items[at0003]/value as triggerPersonal,f/data[at0001]/items[at0004]/items[at0005]/value as triggerFamily,f/data[at0001]/items[at0004]/items[at0006]/value as triggerSocial,f/data[at0001]/items[at0004]/items[at0007]/value as triggerProfessional,f/data[at0001]/items[at0004]/items[at0009]/value as triggerOther,g/data[at0001]/items[at0002]/value as sleepingTime,g/data[at0001]/items[at0007]/value as wakingTime, g/data[at0001]/items[at0012]/value as sleepExperience,g/data[at0001]/items[at0013]/value as wakeupFeeling, g/data[at0001]/items[at0014]/ value as sleepPosition,d/data[at0001]/items[at0.13]/value as socialLife,d/data[at0001]/items[at0.18]/value as abilityToExpress,d/data[at0001]/items[at0.19]/value as professionalCareer, d/data[at0001]/items[at0.7]/value as travelOften, d/data[at0001]/items[at0.8]/value as familyRelationship, h/data[at0001]/events[at0002]/data[at0003]/items[at0.15]/value as frequency, h/data[at0001]/events[at0002]/data[at0003]/items[at0.19]/value as timing, h/data[at0001]/events[at0002]/data[at0003]/items[at0.31]/items[at0.32]/value as bowelPain, h/data[at0001]/events[at0002]/data[at0003]/items[at0.31]/items[at0.33]/value as bowelBlood, h/data[at0001]/events[at0002]/data[at0003]/items[at0.31]/items[at0.34]/value as bowelMucous, h/data[at0001]/events[at0002]/data[at0003]/items[at0.31]/items[at0.35]/value as bowelSmell, h/data[at0001]/events[at0002]/data[at0003]/items[at0005.1]/value as stoolNature, a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value = '{{ehrId}}' ] CONTAINS COMPOSITION a[openEHR-EHR-COMPOSITION.review.v1] CONTAINS SECTION b[openEHR-EHR-SECTION.wellness_summary_rcp.v1] CONTAINS (EVALUATION c[openEHR-EHR-EVALUATION.general_wellness.v0] OR EVALUATION d[openEHR-EHR-EVALUATION.social_summary-extended.v1] OR EVALUATION f[openEHR-EHR-EVALUATION.mental_stress.v0] OR EVALUATION g[openEHR-EHR-EVALUATION.sleep.v0] OR OBSERVATION h[openEHR-EHR-OBSERVATION.bristol_stool_scale-bowel_movements.v1])"
}
```
