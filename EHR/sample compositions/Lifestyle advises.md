# TemplateId - EHRN Lifestyle advises.v0
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Synopsis | Text | Clinical notes | |

## Sample composition

- templateId : EHRN Lifestyle advises.v0
- format : FLAT


```json
{
"ctx/language": "en",
"lifestyle_advice/territory|code": "IN",
"lifestyle_advice/territory|terminology": "ISO_3166-1",
"lifestyle_advice/language|code": "en",
"lifestyle_advice/language|terminology": "ISO_639-1",
"lifestyle_advice/composer|id": "1234-5678", 
"lifestyle_advice/composer|id_scheme": "UUID", 
"lifestyle_advice/composer|id_namespace": "EHR.NETWORK", 
"lifestyle_advice/composer|name": "Dileep",
"lifestyle_advice/context/health_care_facility|id": "123456-123",
"lifestyle_advice/context/health_care_facility|id_scheme": "UUID",
"lifestyle_advice/context/health_care_facility|id_namespace": "EHR.NETWORK",
"lifestyle_advice/context/health_care_facility|name": "HealtheLife",
"lifestyle_advice/context/setting|code": "228",
"lifestyle_advice/context/setting|value": "primary medical care",
"lifestyle_advice/context/setting|terminology": "openehr",
"lifestyle_advice/context/ehrn_metadata:0/confidentiality_level": "5",
"lifestyle_advice/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"lifestyle_advice/context/ehrn_metadata:0/program_details/tenant_id": "ayushehr",
"lifestyle_advice/context/ehrn_metadata:0/program_details/application_name": "OP",
"lifestyle_advice/context/ehrn_metadata:0/program_details/application_id": "op",
"lifestyle_advice/clinical_synopsis:0/synopsis": "Clinical notes"
}



```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Lifestyle advises.v0
- personId : 

#### Body : 
```json

{

    "name": "lifestyle_advises_aql"
    }

```
or
```json

{

    "name": "lifestyle_advises_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    }
```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as createdTime, b/data[at0001]/items[at0002]/value as synopsis, a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value ='{{ehrId}}' ] CONTAINS COMPOSITION a[openEHR-EHR-COMPOSITION.encounter.v1] CONTAINS EVALUATION b[openEHR-EHR-EVALUATION.clinical_synopsis.v1]"
}
```
