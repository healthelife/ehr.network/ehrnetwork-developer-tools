# TemplateId -  EHRN Activity summary.v1
This template records a person's summary of physical activity. It has significance across a patient's clinical encounters. Activity Summary monitoring has the potential to engage patients as advocates in their personalized care, as well as offer health care providers real world assessments of their patients’ daily activity patterns.
## Data points 
The following is a subset of data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Type | coded_text | Yoga  | Local set - Yoga; Meditation; Physical |
| 2 | Activity level | coded_text | First Timer | Local set - First Timer: Beginner; Intermediate; Advanced  | 
| 3 | Description | Text | Jogging & stretching | |
| 4 | Typical frequency | Frequency | 3/wk | |
| 5 | Typical duration | Duration | 45.0 min | |

## Sample composition
- templateId : EHRN Activity summary.v1
- format : FLAT

```json
{
"ctx/language": "en",
"activity_summary/territory|code": "IN",
"activity_summary/territory|terminology": "ISO_3166-1",
"activity_summary/language|code": "en",
"activity_summary/language|terminology": "ISO_639-1",
"activity_summary/composer|id": "1234-5678", 
"activity_summary/composer|id_scheme": "UUID", 
"activity_summary/composer|id_namespace": "EHR.NETWORK", 
"activity_summary/composer|name": "Dileep",
"activity_summary/context/health_care_facility|id": "123456-123",
"activity_summary/context/health_care_facility|id_scheme": "UUID",
"activity_summary/context/health_care_facility|id_namespace": "EHR.NETWORK",
"activity_summary/context/health_care_facility|name": "HealtheLife",
"activity_summary/context/setting|code": "228",
"activity_summary/context/setting|value": "primary medical care",
"activity_summary/context/setting|terminology": "openehr",
"activity_summary/context/ehrn_metadata:0/confidentiality_level": "5",
"activity_summary/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"activity_summary/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
"activity_summary/context/ehrn_metadata:0/program_details/application_name": "Resort",
"activity_summary/context/ehrn_metadata:0/program_details/application_id": "234567-89",
"activity_summary/activity_summary:0/type": "Yoga",
"activity_summary/activity_summary:0/activity_level": "First Timer",
"activity_summary/activity_summary:0/description": "Jogging & stretching",
"activity_summary/activity_summary:0/typical_frequency|magnitude": "3.0",
"activity_summary/activity_summary:0/typical_frequency|unit": "1/wk",
"activity_summary/activity_summary:0/typical_duration|magnitude": "45.0",
"activity_summary/activity_summary:0/typical_duration|unit": "min"
}


```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Activity summary.v1
- personId : 

#### Body : 
```json
{

    "name": "physical_activity_aql"
    }


```
or
```json
{
        "name": "physical_activity_matches_compositionIDs_aql",
        "query-parameters": {
            "CompositionIDList": "{'bd625cf0-8067-4366-9194-65b549409601::local.ethercis.com::1'}"
        }
}
```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as createdTime, b/data[at0001]/items[at0002]/value as activityLevel, b/data[at0001]/items[at0003]/value as description, b/data[at0001]/items[at0007]/value/magnitude as typicalDuration, b/data[at0001]/items[at0007]/value/units as typicalDurationUnit, b/data[at0001]/items[at0008]/value/magnitude as typicalFrequency, b/data[at0001]/items[at0008]/value/units as typicalFrequencyUnit, b/data[at0001]/items[at0030]/value as type, a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value ='{{ehrId}}'] CONTAINS COMPOSITION a[openEHR-EHR-COMPOSITION.activity_summary.v0] CONTAINS EVALUATION b[openEHR-EHR-EVALUATION.activity_summary.v0]"
}
```
