# TemplateId - EHRN Food preference.v0
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Details |
|---|-----|-----------|---------|---------|
| 1 | Diet preference | Text | Vegan |  |
| 2 | Description | Text | Likes only vegan food | |
| 3 | Low carb | Boolean | false | |
| 4 | Gluten free | Boolean | false | |
| 5 | Indian | Boolean | false | |
| 6 | Detox | Boolean | false | |
| 7 | Diet recommendation | Text | Avoid milk | |


## Sample composition
- templateId : EHRN Food preference.v0
- format : FLAT
```json
{
"ctx/language": "en",
"food_preference/territory|code": "IN",
"food_preference/territory|terminology": "ISO_3166-1",
"food_preference/language|code": "en",
"food_preference/language|terminology": "ISO_639-1",
"food_preference/composer|id": "1234-5678",
"food_preference/composer|id_scheme": "UUID",
"food_preference/composer|id_namespace": "EHR.NETWORK",
"food_preference/composer|name": "Dileep",
"food_preference/context/health_care_facility|id": "123456-123",
"food_preference/context/health_care_facility|id_scheme": "UUID",
"food_preference/context/health_care_facility|id_namespace": "EHR.NETWORK",
"food_preference/context/health_care_facility|name": "HealtheLife",
"food_preference/context/setting|code": "232",
"food_preference/context/setting|value": "secondary medical care",
"food_preference/context/setting|terminology": "openehr",
"food_preference/context/ehrn_metadata:0/confidentiality_level": "5",
"food_preference/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"food_preference/context/ehrn_metadata:0/program_details/tenant_id": "ayushehr",
"food_preference/context/ehrn_metadata:0/program_details/application_name": "Resort",
"food_preference/context/ehrn_metadata:0/program_details/application_id": "Resort",
"food_preference/diet_preference:0/diet_preference": "Vegan",
"food_preference/diet_preference:0/diet_preference_details/low_carb": false,
"food_preference/diet_preference:0/diet_preference_details/gluten_free": false,
"food_preference/diet_preference:0/diet_preference_details/indian": false,
"food_preference/diet_preference:0/diet_preference_details/detox": false,
"food_preference/diet_preference:0/description": "Likes only vegan food",
"food_preference/diet_preference:0/diet_recommendation": "Avoid milk"
}



```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Food preference.v0
- personId : 

#### Body : 
```json

{

    "name": "food_preference_aql"
    }


```
or
```json

{

    "name": "food_preference_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    }

```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as created_time,b1/data[at0001]/events[at0002]/data[at0003]/items[at0066]/items[at0081]/items[at0067]/value as dietPreference,b1/data[at0001]/events[at0002]/data[at0003]/items[at0066]/items[at0077]/value as description,b1/data[at0001]/events[at0002]/data[at0003]/items[at0066]/items[at0078]/value as dietRecommendation,b1/data[at0001]/events[at0002]/data[at0003]/items[at0066]/items[at0081]/items[at0082]/value as lowCarb,b1/data[at0001]/events[at0002]/data[at0003]/items[at0066]/items[at0081]/items[at0084]/value as detox,b1/data[at0001]/events[at0002]/data[at0003]/items[at0066]/items[at0081]/items[at0083]/value as glutenFree,b1/data[at0001]/events[at0002]/data[at0003]/items[at0066]/items[at0081]/items[at0085]/value as onlyIndian,b2/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value as dietPreference,b2/data[at0001]/events[at0002]/data[at0003]/items[at0005]/items[at0006]/value as lowCarb,b2/data[at0001]/events[at0002]/data[at0003]/items[at0005]/items[at0007]/value as glutenFree,b2/data[at0001]/events[at0002]/data[at0003]/items[at0005]/items[at0008]/value as onlyIndian,b2/data[at0001]/events[at0002]/data[at0003]/items[at0005]/items[at0009]/value as detox,b2/data[at0001]/events[at0002]/data[at0003]/items[at0010]/value as description,b2/data[at0001]/events[at0002]/data[at0003]/items[at0011]/value as dietRecommendation,a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value ='{{ehrId}}'] CONTAINS COMPOSITION a CONTAINS (OBSERVATION b1[openEHR-EHR-OBSERVATION.food_habits.v0] OR OBSERVATION b2[openEHR-EHR-OBSERVATION.diet_preference.v1])"
}
```
