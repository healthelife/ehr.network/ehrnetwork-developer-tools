# TemplateId - EHRN Samprapti ghataka.v0
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Dosha | coded_text | Vata | Archetype set - Vata (at0017); Pitta (at0018); Kapha (at0019); Vata-pitta (at0020); Pitta-kapha (at0021); Vata-kapha (at0022); Vata-pitta-kapha (at0023) |
| 2 | Dhatu | Text | Rakta, asthi, Mamsa | | 
| 3 | Agni | Text | Agni | |
| 4 | Ama | Text | Ama | |
| 5 | Srotas | Text | Rakthavaha | |
| 6 | Dushti prakara | Text | Ati pravithi | Local set - Ati pravithi; Sanga; Vimarga gamana; Sira granthi | 
| 7 | Utbhava sthana | Text | Utbhava stana | |
| 8 | Sanchara sthana | Text | sanchara stana | |
| 9 | Dosha gati | Text | Dosha gati | |
| 10 | Roga marga | Text | Bahya | Local set - Bahya; Abhyantara; Madhyama | 
| 11 | Roga avastha | Text | Chronic | |
| 12 | Sadhya/asadhya | Text | Sadhya | |
| 13 | Clinical synopsis | Text | The problem can be cured with proper care | |

## Sample composition

- templateId : EHRN Samprapti ghataka.v0
- format : FLAT 

```json
{
"ctx/language": "en",
"samprapti_ghataka/territory|code": "IN",
"samprapti_ghataka/territory|terminology": "ISO_3166-1",
"samprapti_ghataka/language|code": "en",
"samprapti_ghataka/language|terminology": "ISO_639-1",
"samprapti_ghataka/composer|id": "1234-5678", 
"samprapti_ghataka/composer|id_scheme": "UUID", 
"samprapti_ghataka/composer|id_namespace": "EHR.NETWORK", 
"samprapti_ghataka/composer|name": "Dileep",
"samprapti_ghataka/context/health_care_facility|id": "123456-123",
"samprapti_ghataka/context/health_care_facility|id_scheme": "UUID",
"samprapti_ghataka/context/health_care_facility|id_namespace": "EHR.NETWORK",
"samprapti_ghataka/context/health_care_facility|name": "HealtheLife",
"samprapti_ghataka/context/setting|code": "232",
"samprapti_ghataka/context/setting|value": "secondary medical care",
"samprapti_ghataka/context/setting|terminology": "openehr",
"samprapti_ghataka/context/ehrn_metadata:0/confidentiality_level": "5",
"samprapti_ghataka/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"samprapti_ghataka/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
"samprapti_ghataka/context/ehrn_metadata:0/program_details/application_name": "Resort",
"samprapti_ghataka/context/ehrn_metadata:0/program_details/application_id": "234567-89", 
"samprapti_ghataka/samprapti_ghataka:0/dosha|code": "at0017",
"samprapti_ghataka/samprapti_ghataka:0/dosha|value": "Vata",
"samprapti_ghataka/samprapti_ghataka:0/dosha|terminology": "local",
"samprapti_ghataka/samprapti_ghataka:0/dhatu:0": "Rakta, asthi, Mamsa",
"samprapti_ghataka/samprapti_ghataka:0/agni": "Agni text",
"samprapti_ghataka/samprapti_ghataka:0/ama:0": "Ama text",
"samprapti_ghataka/samprapti_ghataka:0/sroto_dushti:0/srotas": "Rakthavaha",
"samprapti_ghataka/samprapti_ghataka:0/sroto_dushti:1/srotas": "Mamsavaha",
"samprapti_ghataka/samprapti_ghataka:0/sroto_dushti:2/srotas": "Asthivaha",
"samprapti_ghataka/samprapti_ghataka:0/sroto_dushti:0/dushti_prakara": "Ati pravithi",
"samprapti_ghataka/samprapti_ghataka:0/utbhava_sthana": "Utbhava stana",
"samprapti_ghataka/samprapti_ghataka:0/sanchara_sthana": "sanchara stana",
"samprapti_ghataka/samprapti_ghataka:0/vyakta_sthana": "Vyakta stana",
"samprapti_ghataka/samprapti_ghataka:0/dosha_gati": "Dosha gati",
"samprapti_ghataka/samprapti_ghataka:0/roga_marga": "Bahya",
"samprapti_ghataka/samprapti_ghataka:0/roga_avastha": "Chronic",
"samprapti_ghataka/samprapti_ghataka:0/sadhya_asadhya": "Sadhya",
"samprapti_ghataka/samprapti_ghataka:0/clinical_synopsis": "The problem can be cured with proper care"
}




```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Samprapti ghataka.v0
- personId : 

#### Body : 
```json

{

    "name": "samprapthi_ghataka_aql"
    }

```
or
```json

{

    "name": "samprapthi_ghataka_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1'}"
        }
    }
```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as date_created, a/uid/value as uid,b/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value as dosha,b/data[at0001]/events[at0002]/data[at0003]/items[at0005]/value as dhatu,b/data[at0001]/events[at0002]/data[at0003]/items[at0006]/value as agni,b/data[at0001]/events[at0002]/data[at0003]/items[at0007]/value as ama,b/data[at0001]/events[at0002]/data[at0003]/items[at0010]/value as utbhavaSthana,b/data[at0001]/events[at0002]/data[at0003]/items[at0011]/value as sancharaSthana,b/data[at0001]/events[at0002]/data[at0003]/items[at0012]/value as vyaktaSthana,b/data[at0001]/events[at0002]/data[at0003]/items[at0013]/value as doshaGati,b/data[at0001]/events[at0002]/data[at0003]/items[at0014]/value as rogaMarga,b/data[at0001]/events[at0002]/data[at0003]/items[at0015]/value as rogaAvastha,b/data[at0001]/events[at0002]/data[at0003]/items[at0016]/value as sadhyaAsadhya,b/data[at0001]/events[at0002]/data[at0003]/items[at0026]/items[at0027]/value as srotas,b/data[at0001]/events[at0002]/data[at0003]/items[at0026]/items[at0028]/value as dushtiPrakara,b/data[at0001]/events[at0002]/data[at0003]/items[at0036]/value as clinicalSynopsis,a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value = '{{ehrId}}' ] CONTAINS COMPOSITION a CONTAINS OBSERVATION b[openEHR-EHR-OBSERVATION.samprapti_ghataka.v0]"
}
```
