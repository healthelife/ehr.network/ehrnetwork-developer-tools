# TemplateId - EHRN Problem list.v1
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Details |
|---|-----|-----------|---------|---------|
| 1 | Problem/Diagnosis name | Coded text | Chronic schizophrenia | |
| 2 | Severity | Coded text | Mild | Archetype set - Mild (at0047); Moderate (at0048); Severe (at0049) |
| 3 | Date/time of onset | Datetime | 2019-05-30T00:00:00.000Z | |
| 4 | Resolution phase | Coded text | Resolving | Archetype set - Not resolving (at0086); Resolving (at0085); Resolved (at0084); Indeterminate (at0087); Relapsed (at0097) |
| 5 | Date/time of resolution | Datetime | 2019-10-20T00:00:00.000Z | |
| 6 | Clinical description | Text | Physical violence since 7 days | |

## Sample composition

- templateId : EHRN Problem list.v1
- format : FLAT

```json
{
"ctx/language": "en",
"problem_list/territory|code": "IN",
"problem_list/territory|terminology": "ISO_3166-1",
"problem_list/language|code": "en",
"problem_list/language|terminology": "ISO_639-1",
"problem_list/composer|id": "1234-5678",
"problem_list/composer|id_scheme": "UUID",
"problem_list/composer|id_namespace": "EHR.NETWORK",
"problem_list/composer|name": "Dileep",
"problem_list/context/health_care_facility|id": "123456-123",
"problem_list/context/health_care_facility|id_scheme": "UUID",
"problem_list/context/health_care_facility|id_namespace": "EHR.NETWORK",
"problem_list/context/health_care_facility|name": "HealtheLife",
"problem_list/context/setting|code": "232",
"problem_list/context/setting|value": "secondary medical care",
"problem_list/context/setting|terminology": "openehr",
"problem_list/context/ehrn_metadata:0/confidentiality_level": "6",
"problem_list/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"problem_list/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
"problem_list/context/ehrn_metadata:0/program_details/application_name": "Resort",
"problem_list/context/ehrn_metadata:0/program_details/application_id": "234567-89",
"problem_list/problem_diagnosis:0/problem_diagnosis_name|value": "Chronic schizophrenia",
"problem_list/problem_diagnosis:0/problem_diagnosis_name|code": "83746006",
"problem_list/problem_diagnosis:0/problem_diagnosis_name|terminology": "SNOMED CT",
"problem_list/problem_diagnosis:0/severity|code": "at0047",
"problem_list/problem_diagnosis:0/severity|value": "Mild",
"problem_list/problem_diagnosis:0/severity|terminology": "local",
"problem_list/problem_diagnosis:0/date_time_of_onset": "2019-05-30T00:00:00.000Z",
"problem_list/problem_diagnosis:0/problem_diagnosis_qualifier:0/resolution_phase|code": "at0085",
"problem_list/problem_diagnosis:0/problem_diagnosis_qualifier:0/resolution_phase|value": "Resolving",
"problem_list/problem_diagnosis:0/problem_diagnosis_qualifier:0/resolution_phase|terminology": "local",
"problem_list/problem_diagnosis:0/date_time_of_resolution": "2019-10-20T00:00:00.000Z",
"problem_list/problem_diagnosis:0/clinical_description": "Physical violence since 7 days"
}

```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Problem list.v1
- personId : 

#### Body : 
```json

{

    "name": "problem_list_aql"
    }

```
or
```json

{

    "name": "problem_list_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    }

```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as date_created,b/data[at0001]/items[at0002]/value as diagnosis,b/data[at0001]/items[at0002]/value/defining_code/code_string as diagnosisCode,b/data[at0001]/items[at0002]/value/defining_code/terminology_id/value as diagnosisTerminology,b/data[at0001]/items[at0005]/value as severity,b/data[at0001]/items[at0005]/value/defining_code/code_string as severityCode,b/data[at0001]/items[at0005]/value/defining_code/terminology_id/value as severityTerminology,b/data[at0001]/items[at0009]/value as description,b/data[at0001]/items[at0077]/value as onsetDate,c/items[at0083]/value as resolutionPhase,c/items[at0083]/value/defining_code/code_string as resolutionPhaseCode,c/items[at0083]/value/defining_code/terminology_id/value as resolutionPhaseTerminology,b/data[at0001]/items[at0030]/value as resolutionDate,a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value ='{{ehrId}}' ] CONTAINS COMPOSITION a CONTAINS EVALUATION b[openEHR-EHR-EVALUATION.problem_diagnosis.v1] CONTAINS CLUSTER c[openEHR-EHR-CLUSTER.problem_qualifier.v1]"
}
```
