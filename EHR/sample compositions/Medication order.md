# TemplateId - EHRN Medication order.v2
This template is designed to record the prescriptions. It is quite extensive and can be used to record the medication orders in a flexible manner.
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Medication item | Text | Atenolol 40 mg tablet  | SNOMED CT/Ayurveda DDB or text entry. Eg. Atenolol 40 mg tablet |
| 2 | Daily Timing description | coded_text | 1-0-1 | | 
| 3 | Direction duration | Text | 4 weeks | |
| 4 | Overall directions description | Text | Avoid fried and spicy food | |
| 5 | Specific directions description | Text | After Food | |

## Sample composition

- templateId : EHRN Medication order.v2
- format : ECISFLAT

```json
{
"/language": "en",
"/territory": "IN",    
"/composer|identifier": "123456-1::UUID::EHR.Network::UUID",
"/composer|name": "Dileep",
"/context/health_care_facility|identifier": "123456-1::UUID::EHR.Network::UUID",
"/context/health_care_facility|name": "HealtheLife",
"/context/setting": "openehr::232|secondary medical care|",
"/context/start_time": "2019-06-08T05:30:00.000+05:30",
"/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]|value": "5", 
"/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0006]/items[at0002]|value": "AyushEHR",  
"/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0006]/items[at0003]|value": "234567-89",  
"/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0006]/items[at0004]|value": "OP",  
"/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0006]/items[at0005]|value": "234567-89",
"/content[openEHR-EHR-SECTION.medication_order_list.v0]/items[openEHR-EHR-INSTRUCTION.medication_order.v2]/activities[at0001]/description[at0002]/items[at0070]|value": "Atenolol 40 mg tablet", 
"/content[openEHR-EHR-SECTION.medication_order_list.v0]/items[openEHR-EHR-INSTRUCTION.medication_order.v2]/activities[at0001]/description[at0002]/items[openEHR-EHR-CLUSTER.therapeutic_direction.v1]/items[openEHR-EHR-CLUSTER.dosage.v1]/items[openEHR-EHR-CLUSTER.timing_daily.v1]/items[at0027]|value": "1-0-1",
"/content[openEHR-EHR-SECTION.medication_order_list.v0]/items[openEHR-EHR-INSTRUCTION.medication_order.v2]/activities[at0001]/description[at0002]/items[openEHR-EHR-CLUSTER.therapeutic_direction.v1]/items[at0066]|value": "@2|P4D",
"/content[openEHR-EHR-SECTION.medication_order_list.v0]/items[openEHR-EHR-INSTRUCTION.medication_order.v2]/activities[at0001]/description[at0002]/items[at0009]|value": "Avoid fried and spicy food",
"/content[openEHR-EHR-SECTION.medication_order_list.v0]/items[openEHR-EHR-INSTRUCTION.medication_order.v2]/activities[at0001]/description[at0002]/items[at0173]|value": "After Food"
}



```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Medication order.v2
- personId : 

#### Body : 
```json

{

    "name": "medication_order_aql"
    }


```
or
```json

{

    "name": "medication_order_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    }
```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as createdTime, c/activities[at0001]/description[at0002]/items[at0070]/value as medicationItem, e_3/items[at0027]/value as timingDescription, e_1/items[at0066]/value as directionDuration, c/activities[at0001]/description[at0002]/items[at0009]/value as overallDirections, c/activities[at0001]/description[at0002]/items[at0173]/value as specificDirections, a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialityLevel FROM EHR e[ehr_id/value = '{{ehrId}}' ] CONTAINS COMPOSITION a[openEHR-EHR-COMPOSITION.prescription.v0] CONTAINS SECTION b[openEHR-EHR-SECTION.medication_order_list.v0] CONTAINS INSTRUCTION c[openEHR-EHR-INSTRUCTION.medication_order.v2] CONTAINS CLUSTER e_1[openEHR-EHR-CLUSTER.therapeutic_direction.v1]CONTAINS CLUSTER e_2[openEHR-EHR-CLUSTER.dosage.v1] CONTAINS CLUSTER e_3[openEHR-EHR-CLUSTER.timing_daily.v1]]"
}
```
