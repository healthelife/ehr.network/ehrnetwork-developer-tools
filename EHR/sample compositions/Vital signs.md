# TemplateId - EHRN Vital signs.v1
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample |
|---|-----|-----------|---------|
| 1 | Systolic BP | Qty | 120 mm[HG] |
| 2 | Diastolic BP | Qty | 80 mm[HG] |
| 3 | Pulse rate | Qty | 72 /min |
| 4 | Temperature | Qty | 35 Cel |
| 5 | Respiration rate | Qty | 25 /min |
| 6 | SPo2 | % | 97 |
| 7 | Clinical synopsis | Text |  |

## Sample composition

- templateId : EHRN Vital signs.v1
- format : FLAT

```json
{
  "ctx/language": "en",
  "vital_signs/territory|code": "IN",
  "vital_signs/territory|terminology": "ISO_3166-1",
  "vital_signs/language|code": "en",
  "vital_signs/language|terminology": "ISO_639-1",
  "vital_signs/composer|id": "1234-5678",
  "vital_signs/composer|id_scheme": "UUID",
  "vital_signs/composer|id_namespace": "EHR.NETWORK",
  "vital_signs/composer|name": "Dileep",
  "vital_signs/context/health_care_facility|id": "123456-123",
  "vital_signs/context/health_care_facility|id_scheme": "UUID",
  "vital_signs/context/health_care_facility|id_namespace": "EHR.NETWORK",
  "vital_signs/context/health_care_facility|name": "HealtheLife",
  "vital_signs/context/setting|code": "232",
  "vital_signs/context/setting|value": "secondary medical care",
  "vital_signs/context/setting|terminology": "openehr",
  "vital_signs/context/ehrn_metadata:0/confidentiality_level": "5",
  "vital_signs/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
  "vital_signs/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
  "vital_signs/context/ehrn_metadata:0/program_details/application_name": "Resort",
  "vital_signs/context/ehrn_metadata:0/program_details/application_id": "234567-89",
  "vital_signs/vital_signs:0/blood_pressure:0/any_event:0/systolic|magnitude": 120,
  "vital_signs/vital_signs:0/blood_pressure:0/any_event:0/systolic|unit": "mm[Hg]",
  "vital_signs/vital_signs:0/blood_pressure:0/any_event:0/diastolic|magnitude": 80,
  "vital_signs/vital_signs:0/blood_pressure:0/any_event:0/diastolic|unit": "mm[Hg]",
  "vital_signs/vital_signs:0/pulse_heart_beat:0/any_event:0/pulse_rate|magnitude": 72,
  "vital_signs/vital_signs:0/pulse_heart_beat:0/any_event:0/pulse_rate|unit": "/min",
  "vital_signs/vital_signs:0/body_temperature:0/any_event:0/temperature|magnitude": 35,
  "vital_signs/vital_signs:0/body_temperature:0/any_event:0/temperature|unit": "Cel",
  "vital_signs/vital_signs:0/respirations:0/any_event:0/rate|magnitude": 25,
  "vital_signs/vital_signs:0/respirations:0/any_event:0/rate|unit": "/min",
  "vital_signs/vital_signs:0/pulse_oximetry:0/any_event:0/spo|numerator": 97,
  "vital_signs/vital_signs:0/pulse_oximetry:0/any_event:0/spo|denominator": 100,
  "vital_signs/vital_signs:0/clinical_synopsis/synopsis": "All vitals normal",
  "vital_signs/vital_signs:0/blood_pressure:0/any_event:0/time": "2019-05-30T00:00:00.000Z",
  "vital_signs/vital_signs:0/pulse_heart_beat:0/any_event:0/time": "2019-05-30T00:00:00.000Z",
  "vital_signs/vital_signs:0/body_temperature:0/any_event:0/time": "2019-05-30T00:00:00.000Z",
  "vital_signs/vital_signs:0/respirations:0/any_event:0/time": "2019-05-30T00:00:00.000Z",
  "vital_signs/vital_signs:0/pulse_oximetry:0/any_event:0/time": "2019-05-30T00:00:00.000Z"
}
```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Vital signs.v1
- personId : 

#### Body : 
```json
{

    "name": "vitals_aql"
    }


```
or
```json
{

    "name": "vitals_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    }
```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as createdTime, g/data[at0001]/items[at0002]/value as synopsis,  b/data[at0001]/events[at0006]/data[at0003]/items[at0004]/value/magnitude as systolicBp, b/data[at0001]/events[at0006]/data[at0003]/items[at0004]/value/units as systolicBpUnit, b/data[at0001]/events[at0006]/data[at0003]/items[at0005]/value/magnitude as diastolicBp, b/data[at0001]/events[at0006]/data[at0003]/items[at0005]/value/units as diastolicBpUnit, c/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/magnitude as pulse, c/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/units as pulseUnit, f/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value/magnitude as respirationRate, f/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value/units as respirationRateUnit, d/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/magnitude as temperature, d/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/units as temperatureUnit, a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value = '{{ehrId}}' ] CONTAINS COMPOSITION a[openEHR-EHR-COMPOSITION.encounter.v1] CONTAINS (OBSERVATION b[openEHR-EHR-OBSERVATION.blood_pressure.v2] OR OBSERVATION c[openEHR-EHR-OBSERVATION.pulse.v1] OR OBSERVATION d[openEHR-EHR-OBSERVATION.body_temperature.v2] OR OBSERVATION f[openEHR-EHR-OBSERVATION.respiration.v1] OR EVALUATION g[openEHR-EHR-EVALUATION.clinical_synopsis.v1])"
}
```
