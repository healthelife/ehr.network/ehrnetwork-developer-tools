# TemplateId - EHRN Imaging examinations result.v0
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Examination result name | Text | CT of entire aorta with contrast  | Code using SNOMED CT? |
| 2 | Findings | coded_text | Findings | | 
| 3 | Clinical information provided | Text | Clinical information | |
| 4 | DateTime result issued | Date | | |
| 5 | Imaging diagnosis | Text | Imaging diagnosis | |
| 6 | Conclusion | Text | Severe indigestion and bloating | |

## Sample composition
- templateId : EHRN Imaging examinations result.v0
- format : FLAT 

```json
{
"ctx/language": "en",
"imaging_examination/territory|code": "IN",
"imaging_examination/territory|terminology": "ISO_3166-1",
"imaging_examination/language|code": "en",
"imaging_examination/language|terminology": "ISO_639-1",
"imaging_examination/composer|id": "1234-5678",
"imaging_examination/composer|id_scheme": "UUID",
"imaging_examination/composer|id_namespace": "EHR.NETWORK",
"imaging_examination/composer|name": "Dileep",
"imaging_examination/context/health_care_facility|id": "123456-123",
"imaging_examination/context/health_care_facility|id_scheme": "UUID",
"imaging_examination/context/health_care_facility|id_namespace": "EHR.NETWORK",
"imaging_examination/context/health_care_facility|name": "HealtheLife",
"imaging_examination/context/setting|code": "232",
"imaging_examination/context/setting|value": "secondary medical care",
"imaging_examination/context/setting|terminology": "openehr",
"imaging_examination/context/ehrn_metadata:0/confidentiality_level": "5",
"imaging_examination/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"imaging_examination/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
"imaging_examination/context/ehrn_metadata:0/program_details/application_name": "Resort",
"imaging_examination/context/ehrn_metadata:0/program_details/application_id": "234567-89",
"imaging_examination/imaging_examination_result:0/any_event:0/examination_result_name|value": "CT of entire aorta with contrast",
"imaging_examination/imaging_examination_result:0/any_event:0/examination_result_name|code": "444969006",
"imaging_examination/imaging_examination_result:0/any_event:0/examination_result_name|terminology": "SNOMED CT",
"imaging_examination/imaging_examination_result:0/any_event:0/datetime_result_issued": "2018-01-21T00:00:00.000Z",  
"imaging_examination/imaging_examination_result:0/any_event:0/clinical_information_provided": "Clinical information",
"imaging_examination/imaging_examination_result:0/any_event:0/findings": "Findings",
"imaging_examination/imaging_examination_result:0/any_event:0/imaging_diagnosis:0": "Imaging diagnosis",
"imaging_examination/imaging_examination_result:0/any_event:0/conclusion": "Conclusion"
}


```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Imaging examinations result.v0
- personId : 

#### Body : 
```json

{

    "name": "imaging_exam_results_aql"
    }


```
or
```json

{

    "name": "imaging_exam_results_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    }

```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as createdTime, b/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value as imagingExamName, b/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value/defining_code/code_string as examNameCode, b/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value/defining_code/terminology_id/value as examNameCodeTerminology, b/data[at0001]/events[at0002]/data[at0003]/items[at0008]/value as findings, b/data[at0001]/events[at0002]/data[at0003]/items[at0014]/value as clinicalInformation,b/data[at0001]/events[at0002]/data[at0003]/items[at0020]/value as imagingDiagnosis,b/data[at0001]/events[at0002]/data[at0003]/items[at0021]/value as conclusion,b/data[at0001]/events[at0002]/data[at0003]/items[at0024]/value as resultTime, a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value ='{{ehrId}}' ] CONTAINS COMPOSITION a CONTAINS OBSERVATION b[openEHR-EHR-OBSERVATION.imaging_exam_result.v0]"
}
```
