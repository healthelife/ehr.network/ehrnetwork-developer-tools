# TemplateId - EHRN Clinical notes.v1
Can be used to general unstructured clinical notes.
## Data points 
This template alllows recording one a simgle desciptive clinical note. 

| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Synopsis | Text | Severe indigestion and bloating | |

## Sample composition
- templateId : EHRN Clinical notes.v1
- format : FLAT

```json
{
"ctx/language": "en",
"clinical_notes/territory|code": "IN",
"clinical_notes/territory|terminology": "ISO_3166-1",
"clinical_notes/language|code": "en",
"clinical_notes/language|terminology": "ISO_639-1",
"clinical_notes/composer|id": "1234-5678", 
"clinical_notes/composer|id_scheme": "UUID", 
"clinical_notes/composer|id_namespace": "EHR.NETWORK", 
"clinical_notes/composer|name": "Dileep",
"clinical_notes/context/health_care_facility|id": "123456-123",
"clinical_notes/context/health_care_facility|id_scheme": "UUID",
"clinical_notes/context/health_care_facility|id_namespace": "EHR.NETWORK",
"clinical_notes/context/health_care_facility|name": "HealtheLife",
"clinical_notes/context/setting|code": "228",
"clinical_notes/context/setting|value": "primary medical care",
"clinical_notes/context/setting|terminology": "openehr",
"clinical_notes/context/ehrn_metadata:0/confidentiality_level": "5",
"clinical_notes/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"clinical_notes/context/ehrn_metadata:0/program_details/tenant_id": "ayushehr",
"clinical_notes/context/ehrn_metadata:0/program_details/application_name": "OP",
"clinical_notes/context/ehrn_metadata:0/program_details/application_id": "op",
"clinical_notes/clinical_synopsis:0/synopsis": "Clinical notes"
}
```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Clinical notes.v1
- personId : 

#### Body : 
```json

{

    "name": "clinical_notes_aql"
}

```
or
```json

{
        "name": "clinical_notes_matches_compositionIDs_aql",
        "query-parameters": {
            "CompositionIDList": "{'63f8d999-3c92-4b84-bd55-72fb9bda6444::local.ethercis.com::1'}"
        }
    }

```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as createdTime, b/data[at0001]/items[at0002]/value as synopsis, a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value ='{{ehrId}}' ] CONTAINS COMPOSITION a CONTAINS EVALUATION b[openEHR-EHR-EVALUATION.clinical_synopsis.v1]"
}
```
