
# TemplateId - EHRN Substance use.v1
This composition is intended to be a persistent substances list of a person's substances use. It has significance across a patient's encounters and is curated from information gathered across clinical events. While it is possible to build a solution to curate this automatically, in realife it is often managed by the care team members.
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Substance | coded_text | Caffeine  | Local set - Caffeine; Alcohol; Tobacco; Narcotics |
| 2 | Period of use | Quantity | 2 months | Local set - Months; Years;  | 
| 3 | Frequency | coded_text | Daily use | Archetype set - Daily use (at0006); Weekly use (at0007); Irregular use (at0008); No use (at0009) |
| 4 | Status | coded_text | Past | Archetype set - Current (at0020); Past (at0021) |
| 5 | Details of use | Text | Severe indigestion and bloating | |

## Sample composition

- templateId : EHRN Substance use.v1
- format : FLAT 

```json
{
      "ctx/composer_name": "Dileep",
      "ctx/health_care_facility|id": "123456-123",
      "ctx/health_care_facility|name": "HealtheLife",
      "ctx/id_namespace": "EHR.NETWORK",
      "ctx/id_scheme": "UUID",
      "ctx/language": "en",
      "ctx/territory": "IN",
      "ctx/time": "2020-10-08T14:38:41.291Z",
       "substance_use/context/start_time": "2020-12-23T09:17:56.172Z",
      "substance_use/current_substance_use:0/extended_substance_use:0/substance|code": "0000",
      "substance_use/current_substance_use:0/extended_substance_use:0/substance|value": "Caffeine",
      "substance_use/current_substance_use:0/extended_substance_use:0/substance|terminology": "Uncoded",
      "substance_use/current_substance_use:0/extended_substance_use:0/status|code": "at0021",
      "substance_use/current_substance_use:0/extended_substance_use:0/status|value": "Past",
      "substance_use/current_substance_use:0/extended_substance_use:0/status|terminology": "Local",
      "substance_use/current_substance_use:0/extended_substance_use:0/period_of_use": "P2M",
      "substance_use/current_substance_use:0/extended_substance_use:0/consumption_details/frequency|code": "at0006",
      "substance_use/current_substance_use:0/extended_substance_use:0/consumption_details/frequency|value": "Daily use",
      "substance_use/current_substance_use:0/extended_substance_use:0/consumption_details/frequency|terminology": "Local",
      "substance_use/current_substance_use:0/extended_substance_use:0/consumption_details/details": "Severe indigestion and bloating"
    }


```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Substance use.v1
- personId : 

#### Body : 
```json

{

    "name": "substance_use_aql"
    }


```
or
```json
{

    "name": "substance_use_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    }
```

### AQL

``` json
{"aql":"select a/uid/value as uid,a/composer/name as author,a/context/start_time/value as date_created, b_a/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value/value as substance, b_a/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value/defining_code/code_string as substanceCode, b_a/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value/defining_code/terminology_id/value as substanceTerminology, b_a/data[at0001]/events[at0002]/data[at0003]/items[at0019]/value/value as status, b_a/data[at0001]/events[at0002]/data[at0003]/items[at0019]/value/defining_code/code_string as statusCode, b_a/data[at0001]/events[at0002]/data[at0003]/items[at0019]/value/defining_code/terminology_id/value as statusTerminology, b_a/data[at0001]/events[at0002]/data[at0003]/items[at0010]/items[at0023]/value/value as details, b_a/data[at0001]/events[at0002]/data[at0003]/items[at0010]/items[at0005]/value/value as frequency, b_a/data[at0001]/events[at0002]/data[at0003]/items[at0010]/items[at0005]/value/defining_code/code_string as frequencyCode, b_a/data[at0001]/events[at0002]/data[at0003]/items[at0010]/items[at0005]/value/defining_code/terminology_id/value as frequencyTerminology, b_a/data[at0001]/events[at0002]/data[at0003]/items[at0022]/value/value as periodOfUse from EHR e [ehr_id/value = '{{ehrId}}'] contains COMPOSITION a[openEHR-EHR-COMPOSITION.substance_use.v1] contains (OBSERVATION b_a[openEHR-EHR-OBSERVATION.substance_use_extended.v0])"
}
```
