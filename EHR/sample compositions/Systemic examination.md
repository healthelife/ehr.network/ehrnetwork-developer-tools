# TemplateId - EHRN Systemic examination.v1
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | System of structure examined | Text | Digestive system  | Code using SNOMED CT? |
| 2 | Body site | Text | Large intestine | Code using SNOMED CT? | 
| 3 | No abnormality detected | Boolean | true | |
| 4 | Clinical description | Text | Clinical description of systemic exam of large intestine | |
| 5 | Clinical Interpretation | Text | Everything is OK | |

## Sample composition

- templateId : EHRN Systemic examination.v1
- format :  FLAT

```json
{
"ctx/language": "en",
"systemic_examination/territory|code": "IN",
"systemic_examination/territory|terminology": "ISO_3166-1",
"systemic_examination/language|code": "en",
"systemic_examination/language|terminology": "ISO_639-1",
"systemic_examination/composer|id": "1234-5678", 
"systemic_examination/composer|id_scheme": "UUID", 
"systemic_examination/composer|id_namespace": "EHR.NETWORK", 
"systemic_examination/composer|name": "Dileep",
"systemic_examination/context/health_care_facility|id": "123456-123",
"systemic_examination/context/health_care_facility|id_scheme": "UUID",
"systemic_examination/context/health_care_facility|id_namespace": "EHR.NETWORK",
"systemic_examination/context/health_care_facility|name": "HealtheLife",
"systemic_examination/context/setting|code": "228",
"systemic_examination/context/setting|value": "primary medical care",
"systemic_examination/context/setting|terminology": "openehr",
"systemic_examination/context/ehrn_metadata:0/confidentiality_level": "5",
"systemic_examination/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"systemic_examination/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
"systemic_examination/context/ehrn_metadata:0/program_details/application_name": "Resort",
"systemic_examination/context/ehrn_metadata:0/program_details/application_id": "234567-89",
"systemic_examination/physical_examination_findings:0/any_event:0/examination_findings:0/system_or_structure_examined|value": "Digestive system",
"systemic_examination/physical_examination_findings:0/any_event:0/examination_findings:0/system_or_structure_examined|code": "86762007",
"systemic_examination/physical_examination_findings:0/any_event:0/examination_findings:0/system_or_structure_examined|terminology": "SNOMED CT",  "systemic_examination/physical_examination_findings:0/any_event:0/examination_findings:0/body_site|value": "Large intestine",
"systemic_examination/physical_examination_findings:0/any_event:0/examination_findings:0/body_site|code": "14742008",
"systemic_examination/physical_examination_findings:0/any_event:0/examination_findings:0/body_site|terminology": "SNOMED CT",  
"systemic_examination/physical_examination_findings:0/any_event:0/examination_findings:0/no_abnormality_detected": true,  
"systemic_examination/physical_examination_findings:0/any_event:0/examination_findings:0/clinical_description": "Clinical description of systemic exam of large intestine",  
"systemic_examination/physical_examination_findings:0/any_event:0/examination_findings:0/clinical_interpretation:0": "Everything is OK"
}

```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Systemic examination.v1
- personId : 

#### Body : 
```json

{

    "name": "systemic_examination_aql"
    }

```
or
```json
{

    "name": "systemic_examination_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    }
```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as createdTime, c/items[at0001]/value as systemName, c/items[at0001]/value/defining_code/code_string as systemCode, c/items[at0001]/value/defining_code/terminology_id/value as systemCodeTerminology, c/items[at0012]/value as bodysite, c/items[at0012]/value/defining_code/code_string as bodysiteCode, c/items[at0012]/value/defining_code/terminology_id/value as bodysiteCodeTerminology, c/items[at0002]/value as noAbnormalityDetected, c/items[at0003]/value as clinicalDescription, c/items[at0006]/value as clinicalInterpretation, b/data[at0001]/events[at0002]/time/value as observedTime, a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value = '{{ehrId}}' ] CONTAINS COMPOSITION a[openEHR-EHR-COMPOSITION.encounter.v1] CONTAINS OBSERVATION b[openEHR-EHR-OBSERVATION.exam.v1] CONTAINS CLUSTER c[openEHR-EHR-CLUSTER.exam.v1]"
}
```
