# TemplateId - EHRN Complaints.v2
This template can be used to record the patient narratives and complaints. 
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Details |
|---|-----|-----------|---------|---------|
| 1 | Symptom/Sign name | Text | Acute knee joint pain | Code using SNOMED CT? |
| 2 | Impact | Coded text | Others | Local set - Chief complaint; Associated complaint; Others |
| 3 | Severity category | Coded text | Mild | Local set - Mild; Moderate; Severe |
| 4 | Progression | Coded text | Worsening | Archetype set - Unchanged (at0182); Improving (at0181); Worsening (at0183); Resolved (at0184) |
| 5 | Description | Text | Joint pain from many days | |
| 6 | Episode onset | Date | | |
| 7 | Resolution date/time | Date | | |
## Sample composition
- templateId : EHRN Complaints.v2
- format : FLAT

```json
{
"ctx/language": "en",
"complaints/territory|code": "IN",
"complaints/territory|terminology": "ISO_3166-1",
"complaints/language|code": "en",
"complaints/language|terminology": "ISO_639-1",
"complaints/composer|id": "1234-5678", 
"complaints/composer|id_scheme": "UUID", 
"complaints/composer|id_namespace": "EHR.NETWORK", 
"complaints/composer|name": "Dileep",
"complaints/context/health_care_facility|id": "123456-123",
"complaints/context/health_care_facility|id_scheme": "UUID",
"complaints/context/health_care_facility|id_namespace": "EHR.NETWORK",
"complaints/context/health_care_facility|name": "HealtheLife",
"complaints/context/setting|code": "232",
"complaints/context/setting|value": "secondary medical care",
"complaints/context/setting|terminology": "openehr",
"complaints/context/ehrn_metadata:0/confidentiality_level": "0",
"complaints/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"complaints/context/ehrn_metadata:0/program_details/tenant_id": "ayushehr",
"complaints/context/ehrn_metadata:0/program_details/application_name": "Resort",
"complaints/context/ehrn_metadata:0/program_details/application_id": "resort",
"complaints/story_history:0/symptom_sign:0/symptom_sign_name|value": "Acute knee joint pain",
"complaints/story_history:0/symptom_sign:0/symptom_sign_name|code": "112581000119104",
"complaints/story_history:0/symptom_sign:0/symptom_sign_name|terminology": "SNOMED CT",
"complaints/story_history:0/symptom_sign:0/impact:0": "Chief complaint",
"complaints/story_history:0/symptom_sign:0/severity_category|code": "at0023",
"complaints/story_history:0/symptom_sign:0/severity_category|value": "Mild",
"complaints/story_history:0/symptom_sign:0/severity_category|terminology": "local",
"complaints/story_history:0/symptom_sign:0/progression:0|code": "at0183",
"complaints/story_history:0/symptom_sign:0/progression:0|value": "Worsening",
"complaints/story_history:0/symptom_sign:0/progression:0|terminology": "local",
"complaints/story_history:0/symptom_sign:0/description": "Joint pain from many days",
"complaints/story_history:0/symptom_sign:0/episode_onset": "2019-05-23T00:00:00.000Z",
"complaints/story_history:0/symptom_sign:0/resolution_date_time": "2019-05-23T00:00:00.000Z"
}
```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Complaints.v2
- personId : 

#### Body : 
```json
{

    "application/json": 

{

    "name": "complaints_aql"
    }

}

```
or
```json
{

    "application/json": 

{
        "name": "complaints_matches_compositionIDs_aql",
        "query-parameters": {
            "CompositionIDList": "{'63f8d999-3c92-4b84-bd55-72fb9bda6444::local.ethercis.com::1'}"
        }
    }

}
```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as createdTime, c/items[at0001]/value as complaint, c/items[at0002]/value as description, c/items[at0021]/value as severity, c/items[at0155]/value as impact, c/items[at0180]/value as progression, c/items[at0152]/value as onsetDate, c/items[at0161]/value as resolutionDate,a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value ='{{ehrId}}'] CONTAINS COMPOSITION a[openEHR-EHR-COMPOSITION.encounter.v1] CONTAINS OBSERVATION b[openEHR-EHR-OBSERVATION.story.v1] CONTAINS CLUSTER c[openEHR-EHR-CLUSTER.symptom_sign.v1]"
}
```
