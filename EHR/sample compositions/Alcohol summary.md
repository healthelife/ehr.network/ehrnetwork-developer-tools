# TemplateId - EHRN Alcohol consumption summary.v0
This composition records a person's alcohol consumption summary. This can include curent status or past history.
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Overall status | coded_text | Current drinker  | Archetype set - Current drinker (at0003); Former drinker (at0005); Lifetime non-drinker (at0006)|
| 2 | Overall description | Text | Fairly heavy drinker | | 
| 3 | Type | coded_text | Spirits | Archetype set - Beer (at0115); Wine (at0116); Cider (at0117); Mead (at0118); Pulque (at0119); Spirits (at0120); Fortified wine (at0121) |
| 4 | Typical consumption | Quantity | 5.0/d |  |

## Sample composition
- templateId : EHRN Alcohol consumption summary.v0
- format : ECISFLAT

```json
{
"/language": "en",
"/territory": "IN",    
"/composer|identifier": "123456-1::UUID::EHR.Network::UUID",
"/composer|name": "Dileep",
"/context/health_care_facility|identifier": "123456-1::UUID::EHR.Network::UUID",
"/context/health_care_facility|name": "HealtheLife",
"/context/setting": "openehr::228|primary medical care|",
"/context/start_time": "2019-06-07T22:19:08.233+05:30",
"/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]|value": "5", 
"/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0006]/items[at0002]|value": "AyushEHR",  
"/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0006]/items[at0003]|value": "234567-89",  
"/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0006]/items[at0004]|value": "OP",  
"/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0006]/items[at0005]|value": "234567-89",
"/content[openEHR-EHR-EVALUATION.alcohol_consumption_summary.v1]/data[at0001]/items[at0089]|value": "local::at0003|Current drinker|",
"/content[openEHR-EHR-EVALUATION.alcohol_consumption_summary.v1]/data[at0001]/items[at0043]|value": "Regular Cigerette smoker",
"/content[openEHR-EHR-EVALUATION.alcohol_consumption_summary.v1]/data[at0001]/items[at0064]/items[at0029]/items[at0108]|value": "@2|local::at0120|Spirits|", 
"/content[openEHR-EHR-EVALUATION.alcohol_consumption_summary.v1]/data[at0001]/items[at0064]/items[at0029]/items[at0111]|value": "5.0,1/d"
}



```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Alcohol consumption summary.v0
- personId : 

#### Body : 
```json
{

    "name": "alcohol_summary_aql"
    }


```
or
```json

{

    "name": "alcohol_summary_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    }
```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as createdTime, b/data[at0001]/items[at0089]/value as overallStatus, b/data[at0001]/items[at0043]/value as overallDescription, b/data[at0001]/items[at0064]/items[at0029]/items[at0108]/value as type, b/data[at0001]/items[at0064]/items[at0029]/items[at0111]/value/magnitude as typicalConsumption, a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value ='{{ehrId}}' ] CONTAINS COMPOSITION a[openEHR-EHR-COMPOSITION.lifestyle_factors.v0] CONTAINS EVALUATION b[openEHR-EHR-EVALUATION.alcohol_consumption_summary.v1]"
}
```
