# TemplateId - EHRN Goals.v0
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Goal name | Text | Father  | Reduce pain in calves & ankles |
| 2 | Clinical indication | Text | Osteoarthritis |  | 
| 3 | Goal outcome | Coded text | Achieved | Archetype set : Achieved (at0015); Partially achieved (at0016); Not achieved (at0017) |
| 4 | Goal comment | Text | Pain in calves & ankles reduced | |

## Sample composition
- templateId : EHRN Goals.v0
- format : FLAT 

```json
{
"ctx/language": "en",
"goals/territory|code": "IN",
"goals/territory|terminology": "ISO_3166-1",
"goals/language|code": "en",
"goals/language|terminology": "ISO_639-1",
"goals/composer|id": "1234-5678", 
"goals/composer|id_scheme": "UUID", 
"goals/composer|id_namespace": "EHR.NETWORK", 
"goals/composer|name": "Dileep",
"goals/context/health_care_facility|id": "123456-123",
"goals/context/health_care_facility|id_scheme": "UUID",
"goals/context/health_care_facility|id_namespace": "EHR.NETWORK",
"goals/context/health_care_facility|name": "HealtheLife",
"goals/context/setting|code": "232",
"goals/context/setting|value": "secondary medical care",
"goals/context/setting|terminology": "openehr",
"goals/context/ehrn_metadata:0/confidentiality_level": "5",
"goals/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"goals/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
"goals/context/ehrn_metadata:0/program_details/application_name": "Resort",
"goals/context/ehrn_metadata:0/program_details/application_id": "234567-89",
"goals/goal:0/goal_name": "Reduce pain in calves & ankles",
"goals/goal:0/clinical_indication:0": "Osteoarthritis",
"goals/goal:0/goal_outcome|value": "Achieved",
"goals/goal:0/goal_outcome|code": "at0015",
"goals/goal:0/goal_outcome|terminology": "local",
"goals/goal:0/goal_comment": "Pain in calves & ankles reduced"
}



```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Goals.v0
- personId : 

#### Body : 
```json
{

    "name": "goals_aql"
    }

```
or
```json
{

    "name": "goals_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    }
```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as date_created,b/data[at0001]/items[at0002]/value as goal,b/data[at0001]/items[at0010]/value as clinicalIndication,b/data[at0001]/items[at0013]/value as goalOutcome,b/data[at0001]/items[at0013]/value/defining_code/code_string as goalOutcomeCode,b/data[at0001]/items[at0013]/value/defining_code/terminology_id/value as goalOutcomeTerminology,b/data[at0001]/items[at0022]/value as Comments,a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value ='{{ehrId}}' ] CONTAINS COMPOSITION a CONTAINS EVALUATION b[openEHR-EHR-EVALUATION.goal.v1]"
}
```