# TemplateId - EHRN Service order.v1
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Service name | Text | Abhyangam+svedanam  | Ayurvedic service lists |
| 2 | Service type | Text | Ayurveda treatments | | 
| 2 | Total amount | Count | 3 | | 
| 3 | Direction description | Text | Eladi thailam +murivenna | |
| 4 | Clinical indication | Text | Back pain | |

## Sample composition

- templateId : EHRN Service order.v1
- format : FLAT

```json
{
"ctx/language": "en",
"service_order/territory|code": "IN",
"service_order/territory|terminology": "ISO_3166-1",
"service_order/language|code": "en",
"service_order/language|terminology": "ISO_639-1",
"service_order/composer|id": "1234-5678", 
"service_order/composer|id_scheme": "UUID", 
"service_order/composer|id_namespace": "EHR.NETWORK", 
"service_order/composer|name": "Dileep",
"service_order/context/health_care_facility|id": "123456-123",
"service_order/context/health_care_facility|id_scheme": "UUID",
"service_order/context/health_care_facility|id_namespace": "EHR.NETWORK",
"service_order/context/health_care_facility|name": "HealtheLife",
"service_order/context/setting|code": "232",
"service_order/context/setting|value": "secondary medical care",
"service_order/context/setting|terminology": "openehr",
"service_order/context/ehrn_metadata:0/confidentiality_level": "5",
"service_order/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"service_order/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
"service_order/context/ehrn_metadata:0/program_details/application_name": "Resort",
"service_order/context/ehrn_metadata:0/program_details/application_id": "234567-89", 
"service_order/service_request:0/current_activity:0/service_name": "Abhyangam+svedanam",  
"service_order/service_request:0/current_activity:0/service_type": "Ayurveda treatments",
"service_order/service_request:0/current_activity:0/service_direction:0/total_amount/value": 3,  
"service_order/service_request:0/current_activity:0/service_direction:0/direction_description": "Eladi thailam +murivenna",
"service_order/service_request:0/current_activity:0/clinical_indication:0": "Back pain",
"service_order/service_request:0/current_activity:0/timing": "DEFAULT_TIMING",
"service_order/service_request:0/narrative": "DEFALULT_NARRATIVE"
}




```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Service order.v1
- personId : 

#### Body : 
```json

{

    "name": "service_order_aql"
    }


```
or
```json

{

    "name": "service_order_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    }

```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as date_created,b/activities[at0001]/description[at0009]/items[at0121]/value as serviceName,c/items[at0011]/value/magnitude as counts,c/items[at0002]/value as directionDescription,b/activities[at0001]/description[at0009]/items[at0152]/value as clinicalIndication,a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value = '{{ehrId}}' ] CONTAINS COMPOSITION a CONTAINS INSTRUCTION b[openEHR-EHR-INSTRUCTION.service_request.v1] CONTAINS CLUSTER c[openEHR-EHR-CLUSTER.service_direction.v0]"
}
```
