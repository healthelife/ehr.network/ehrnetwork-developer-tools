# TemplateId - EHRN Menstrual summary.v2
This composition is intended to be a persistent curated list of a person's menstural summary. It has significance across a patient's encounters and is curated from information gathered across clinical events. While it is possible to build a solution to curate this automatically, in realife it is often managed by the care team members.
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Blood clots | Boolean | false  | Values - true; false|
| 2 | Flooding | Boolean | false | Values - true; false | 
| 3 | Menarche age | Number | 14 | |
| 4 | LNMP | Date | 2020-10-07T18:30:00.000Z |  |
| 5 | Menopause age | Number | 50 |  |
| 6 | Length of cycle | Duration | 3 months 2 Weeks 3 days  | PT0S format |
| 7 | Duration of menses | Duration | 3 days |  PT0S format |
| 8 | Qualitative description of menses | coded_text | Heavy | Options - Very heavy (at0016); Heavy (at0017); Normal (at0019); Light (at0020); Spotting (at0021)  |
| 9 | Description of menses | Text | Severe stomachache and cramps |  |

## Sample composition

- templateId : EHRN Menstrual summary.v2
- format : FLAT

```json
{
    "ctx/composer_name": "Dileep",
    "ctx/health_care_facility|id": "123456-123",
    "ctx/health_care_facility|name": "HealtheLife",
    "ctx/id_namespace": "EHR.NETWORK",
    "ctx/id_scheme": "UUID",
    "ctx/language": "en",
    "ctx/territory": "IN",
    "ctx/time": "2020-10-08T14:38:41.291Z",
    "review/context/start_time": "2020-10-08T14:38:41.291Z",
    "review/menstrual_cycle_summary_-_extended:0/menarche_age": "P12Y",
    "review/menstrual_cycle_summary_-_extended:0/last_normal_menstrual_period_lnmp": "2020-10-07T18:30:00.000Z",
    "review/menstrual_cycle_summary_-_extended:0/menopause_age": "P43Y",
    "review/menstrual_cycle_anyevent:0/length_of_cycle": "P3M2W3D",
    "review/menstrual_cycle_anyevent:0/duration_of_menses": "P3D",
    "review/menstrual_cycle_anyevent:0/description_of_menses": "Descritpion",
    "review/menstrual_cycle_anyevent:0/qualitative_description_of_menses|code": "at0017",
    "review/menstrual_cycle_anyevent:0/qualitative_description_of_menses|value": "Heavy",
    "review/menstrual_cycle_anyevent:0/qualitative_description_of_menses|terminology": "Local",
    "review/menstrual_cycle_anyevent:0/blood_clots": true,
    "review/menstrual_cycle_anyevent:0/flooding": true
}



```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Menstrual summary.v2
- personId : 

#### Body : 
```json
{

    "name": "menstural_summary_aql"
    }


```
or
```json

{

    "name": "menstural_summary_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    }
```

### AQL

``` json
{"aql":"select     a/uid/value as uid,a/composer/name as author,a/name/value as template_name,a/context/start_time/value as date_created ,a_a/data[at0001]/items[at0.5]/value/value as menarche,a_a/data[at0001]/items[at0003]/value/value as lnmp,a_a/data[at0001]/items[at0.6]/value/value as menopause,a_b/data[at0001]/events[at0002]/data[at0003]/items[at0010]/value/value as mensesDur,a_b/data[at0001]/events[at0002]/data[at0003]/items[at0011]/value/value as lengthOfCycle,a_b/data[at0001]/events[at0002]/data[at0003]/items[at0022]/value/value as bloodClots,a_b/data[at0001]/events[at0002]/data[at0003]/items[at0023]/value/value as flooding,a_b/data[at0001]/events[at0002]/data[at0003]/items[at0018]/value/value as mensesQualDesc,a_b/data[at0001]/events[at0002]/data[at0003]/items[at0018]/value/defining_code/code_string as mensesQualDescCode,a_b/data[at0001]/events[at0002]/data[at0003]/items[at0018]/value/defining_code/terminology_id/value as mensesQualDescTerminology,a_b/data[at0001]/events[at0002]/data[at0003]/items[at0014]/value/value as mensesDesc FROM EHR e [ehr_id/value = '{{ehrId}}'] CONTAINS COMPOSITION a[openEHR-EHR-COMPOSITION.review.v1]  CONTAINS (EVALUATION a_a[openEHR-EHR-EVALUATION.menstrual_cycle_summary-extended.v0] or OBSERVATION a_b[openEHR-EHR-OBSERVATION.menstrual_cycle.v0])"
}
```
