# TemplateId - EHRN Tobacco smoking summary.v0
This composition is intended to be a persistent curated list of a person's tobacco smoking summary. It has significance across a patient's encounters and is curated from information gathered across clinical events. While it is possible to build a solution to curate this automatically, in realife it is often managed by the care team members.
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Overall status | coded_text | Never smoked  | Archetype set  - Never smoked (at0006); Current smoker (at0003); Former smoker (at0005) |
| 2 | Overall description | Text | Regular Cigerette smoker | | 
| 3 | Type | coded_text | Cigarettes | Archetype set  - Cigarettes (at0054); Hand-rolled cigarettes (at0055); Cigars (at0056); Cigarillos (at0066); Pipe (at0057); Waterpipe (at0062); Bidis (at0078); Kreteks (at0088) |
| 4 | Overall years of smoking | Quantity | 10 |  |
| 5 | Overall comment | Text | %packs per day for 7 years, 2 packs per day for last 2years | |

## Sample composition

- templateId : EHRN Tobacco smoking summary.v0
- format : ECISFLAT 

```json
{
"/language": "en",
"/territory": "IN",    
"/composer|identifier": "123456-1::UUID::EHR.Network::UUID",
"/composer|name": "Dileep",
"/context/health_care_facility|identifier": "123456-1::UUID::EHR.Network::UUID",
"/context/health_care_facility|name": "HealtheLife",
"/context/setting": "openehr::228|primary medical care|",
"/context/start_time": "2019-06-08T22:19:08.233+05:30",
"/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]|value": "5", 
"/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0006]/items[at0002]|value": "AyushEHR",  
"/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0006]/items[at0003]|value": "234567-89",  
"/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0006]/items[at0004]|value": "OP",  
"/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0006]/items[at0005]|value": "234567-89",
"/content[openEHR-EHR-EVALUATION.tobacco_smoking_summary.v1]/data[at0001]/items[at0089]|value": "local::at0006|Never smoked|",
"/content[openEHR-EHR-EVALUATION.tobacco_smoking_summary.v1]/data[at0001]/items[at0043]|value": "Regular Cigerette smoker",
"/content[openEHR-EHR-EVALUATION.tobacco_smoking_summary.v1]/data[at0001]/items[at0029]/items[at0095]|value": "local::at0054|Cigarettes|",
"/content[openEHR-EHR-EVALUATION.tobacco_smoking_summary.v1]/data[at0001]/items[at0093]|value": "10",
"/content[openEHR-EHR-EVALUATION.tobacco_smoking_summary.v1]/data[at0001]/items[at0019]|value": "%packs per day for 7 years, 2 packs per day for last 2years"
}


```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Tobacco smoking summary.v0
- personId : 

#### Body : 
```json
{

    "name": "tobacco_smoking_summary_aql"
    }

```
or
```json
{

    "name": "tobacco_smoking_summary_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    }
```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as createdTime, b/data[at0001]/items[at0019]/value as overallComment,  b/data[at0001]/items[at0029]/items[at0095]/value as type, b/data[at0001]/items[at0043]/value as overallDescription,  b/data[at0001]/items[at0089]/value as overallStatus, b/data[at0001]/items[at0093]/value/magnitude as YearsOfSmoking, a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value = '{{ehrId}}' ] CONTAINS COMPOSITION a[openEHR-EHR-COMPOSITION.lifestyle_factors.v0] CONTAINS EVALUATION b[openEHR-EHR-EVALUATION.tobacco_smoking_summary.v1]"
}
```
