(# TemplateId - EHRN Medication summary.v0
This composition is intended to be a persistent history of a person's medications. It has significance across a patient's encounters and is curated from information gathered across clinical events. While it is possible to build a solution to curate this automatically, in realife it is often managed by the care team members.
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Medication name | Text | Paracetamol 500 mg oral tablet  | Code using SNOMED CT? |
| 2 | Current use? | Boolean | false | | 
| 3 | Clinical description | Text | 1-0-1, after food. For back pain | |
| 4 | Onset of use | Date | 2018-01-25T00:00:00.000Z | |
| 5 | Cessation of use | Date | 2018-10-12T00:00:00.000Z | |
| 6 | Cumulative dose | Quantity | 200.0 mg | Total qty consumed; Qty includes magnitude(200.0) & unit(mg)|

## Sample composition
- templateId : EHRN Medication summary.v0
- format : FLAT
```json
{
"ctx/language": "en",
"medication_summary/territory|code": "IN",
"medication_summary/territory|terminology": "ISO_3166-1",
"medication_summary/language|code": "en",
"medication_summary/language|terminology": "ISO_639-1",
"medication_summary/composer|id": "1234-5678", 
"medication_summary/composer|id_scheme": "UUID", 
"medication_summary/composer|id_namespace": "EHR.NETWORK", 
"medication_summary/composer|name": "Dileep",
"medication_summary/context/health_care_facility|id": "123456-123",
"medication_summary/context/health_care_facility|id_scheme": "UUID",
"medication_summary/context/health_care_facility|id_namespace": "EHR.NETWORK",
"medication_summary/context/health_care_facility|name": "HealtheLife",
"medication_summary/context/setting|code": "228",
"medication_summary/context/setting|value": "primary medical care",
"medication_summary/context/setting|terminology": "openehr",
"medication_summary/context/ehrn_metadata:0/confidentiality_level": "5",
"medication_summary/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"medication_summary/context/ehrn_metadata:0/program_details/tenant_id": "ayushehr",
"medication_summary/context/ehrn_metadata:0/program_details/application_name": "OP",
"medication_summary/context/ehrn_metadata:0/program_details/application_id": "op",
"medication_summary/medication_summary:0/medication_name|value": "Paracetamol 500 mg oral tablet",
"medication_summary/medication_summary:0/medication_name|code": "322236009",
"medication_summary/medication_summary:0/medication_name|terminology": "SNOMED CT",
"medication_summary/medication_summary:0/current_use": false,
"medication_summary/medication_summary:0/clinical_description": "1-0-1, after food. For back pain",
"medication_summary/medication_summary:0/onset_of_use": "2018-01-25T00:00:00.000Z",
"medication_summary/medication_summary:0/cessation_of_use": "2018-10-12T00:00:00.000Z",
"medication_summary/medication_summary:0/cumulative_dose|magnitude": 200.0,
"medication_summary/medication_summary:0/cumulative_dose|unit": "mg"
}


```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Medication summary.v0
- personId : 

#### Body : 
```json

{

    "name": "medication_summary_aql"
    }

```
or
```json

{

    "name": "medication_summary_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    }
```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as createdTime, b/data[at0001]/items[at0002]/value as medicationItem, b/data[at0001]/items[at0004]/value as currentUse, b/data[at0001]/items[at0007]/value as description, b/data[at0001]/items[at0009]/value as onsetOfUse, b/data[at0001]/items[at0010]/value as cessationOfUse, b/data[at0001]/items[at0015]/value/magnitude as quantity, b/data[at0001]/items[at0015]/value/units as unit,b/data[at0001]/items[at0008]/items[at0014]/value as doseDuration,    b/data[at0001]/items[at0008]/items[at0018]/value as therapeuticIntend,a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialityLevel FROM EHR e[ehr_id/value ='{{ehrId}}'] CONTAINS COMPOSITION a CONTAINS EVALUATION b[openEHR-EHR-EVALUATION.medication_summary.v0]"
}
```
