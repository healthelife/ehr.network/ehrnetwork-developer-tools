# TemplateId - EHRN Anthropometry.v2
This template records the different physical measurements of the body. 
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Height/length | Qty(cm) | 165.0 cm  | |
| 2 | Weight | Qty(kg) | 85.0 kg | | 
| 3 | BMI | Qty(Mass/area) | 28.0 kg/m2 | |
| 4 | Hip circumference | Qty(cm) | 100.0 cm | |
| 5 | Left arm circumference | Qty(cm) | 30.0 cm | |
| 6 | Right arm circumference | Qty(cm) | 30.0 cm | |
| 7 | Right thigh circumference | Qty(cm) | 52. cm | |
| 8 | Left thigh circumference | Qty(cm) | 52.0 cm | |
| 9 | Chest circumference | Qty(cm) | 75.0 cm | |
| 10 | Waist circumference | Qty(cm) | 90.0 cm | |

## Sample composition
- templateId : EHRN Anthropometry.v2
- format : FLAT

```json
{
"ctx/language": "en",
"anthropometry/territory|code": "IN",
"anthropometry/territory|terminology": "ISO_3166-1",
"anthropometry/language|code": "en",
"anthropometry/language|terminology": "ISO_639-1",
"anthropometry/composer|id": "1234-5678",
"anthropometry/composer|id_scheme": "UUID",
"anthropometry/composer|id_namespace": "EHR.NETWORK",
"anthropometry/composer|name": "Dileep",
"anthropometry/context/health_care_facility|id": "123456-123",
"anthropometry/context/health_care_facility|id_scheme": "UUID",
"anthropometry/context/health_care_facility|id_namespace": "EHR.NETWORK",
"anthropometry/context/health_care_facility|name": "HealtheLife",
"anthropometry/context/setting|code": "232",
"anthropometry/context/setting|value": "secondary medical care",
"anthropometry/context/setting|terminology": "openehr",
"anthropometry/context/ehrn_metadata:0/confidentiality_level": "5",  
"anthropometry/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"anthropometry/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
"anthropometry/context/ehrn_metadata:0/program_details/application_name": "Resort",
"anthropometry/context/ehrn_metadata:0/program_details/application_id": "234567-89",
"anthropometry/anthropometry:0/height_length:0/any_event:0/height_length|magnitude": 165.0,  
"anthropometry/anthropometry:0/height_length:0/any_event:0/height_length|unit": "cm",   
"anthropometry/anthropometry:0/body_weight:0/any_event:0/weight|magnitude": 85.0,   
"anthropometry/anthropometry:0/body_weight:0/any_event:0/weight|unit": "kg",
"anthropometry/anthropometry:0/arm_circumference:0/any_event:0/left_arm_circumference|magnitude": 30.0,   
"anthropometry/anthropometry:0/arm_circumference:0/any_event:0/left_arm_circumference|unit": "cm",   
"anthropometry/anthropometry:0/arm_circumference:0/any_event:0/right_arm_circumference|magnitude": 30.0,  
"anthropometry/anthropometry:0/arm_circumference:0/any_event:0/right_arm_circumference|unit": "cm",  
"anthropometry/anthropometry:0/chest_circumference:0/any_event:0/chest_circumference|magnitude": 75.0,  
"anthropometry/anthropometry:0/chest_circumference:0/any_event:0/chest_circumference|unit": "cm",
"anthropometry/anthropometry:0/waist_circumference:0/any_event:0/waist_circumference|magnitude": 90.0,   
"anthropometry/anthropometry:0/waist_circumference:0/any_event:0/waist_circumference|unit": "cm",  
"anthropometry/anthropometry:0/hip_circumference:0/any_event:0/hip_circumference|magnitude": 100.0,  
"anthropometry/anthropometry:0/hip_circumference:0/any_event:0/hip_circumference|unit": "cm",   
"anthropometry/anthropometry:0/thigh_circumference:0/any_event:0/left_thigh_circumference|magnitude": 52.0,   
"anthropometry/anthropometry:0/thigh_circumference:0/any_event:0/left_thigh_circumference|unit": "cm",   
"anthropometry/anthropometry:0/thigh_circumference:0/any_event:0/right_thigh_circumference|magnitude": 52.0,   
"anthropometry/anthropometry:0/thigh_circumference:0/any_event:0/right_thigh_circumference|unit": "cm",   
"anthropometry/anthropometry:0/body_mass_index:0/any_event:0/body_mass_index|magnitude": 28.0,  
"anthropometry/anthropometry:0/body_mass_index:0/any_event:0/body_mass_index|unit": "kg/m2"
}


```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 


## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Anthropometry.v2
- personId : 

#### Body : 
```json
{

    "name": "anthropometry_aql"
    }


```
or
```json

{
        "name": "anthropometry_matches_compositionIDs_aql",
        "query-parameters": {
            "CompositionIDList": "{'63f8d999-3c92-4b84-bd55-72fb9bda6444::local.ethercis.com::1'}"
        }
    }

```

### AQL

``` json
{"aql":"select   a/uid/value as uid,    a/composer/name as author,   a/context/start_time/value as date_created,   b_a/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value/magnitude as height,   b_a/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value/units as heightUnit,   b_b/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/magnitude as weight,      b_b/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/units as weightUnit,   b_c/data[at0001]/events[at0010]/data[at0003]/items[at0004]/value/magnitude as leftArmCir,     b_c/data[at0001]/events[at0010]/data[at0003]/items[at0004]/value/units as leftArmCirUnit,   b_c/data[at0001]/events[at0010]/data[at0003]/items[at0013]/value/magnitude as rightArmCir,     b_c/data[at0001]/events[at0010]/data[at0003]/items[at0013]/value/units as rightArmCirUnit,   b_d/data[at0001]/events[at0010]/data[at0003]/items[at0004]/value/magnitude as chestCir,   b_d/data[at0001]/events[at0010]/data[at0003]/items[at0004]/value/units as chestCirUnit,   b_e/data[at0001]/events[at0010]/data[at0003]/items[at0004]/value/magnitude as waistCrm,   b_e/data[at0001]/events[at0010]/data[at0003]/items[at0004]/value/units as waistCrmUnit,   b_f/data[at0001]/events[at0010]/data[at0003]/items[at0004]/value/magnitude as hipCir,      b_f/data[at0001]/events[at0010]/data[at0003]/items[at0004]/value/units as hipCirUnit,   b_g/data[at0001]/events[at0010]/data[at0003]/items[at0004]/value/magnitude as leftThighCir,     b_g/data[at0001]/events[at0010]/data[at0003]/items[at0004]/value/units as leftThighCirUnits,   b_g/data[at0001]/events[at0010]/data[at0003]/items[at0013]/value/magnitude as rightThighCir,   b_g/data[at0001]/events[at0010]/data[at0003]/items[at0013]/value/units as rightThighCirUnit,   b_h/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value/magnitude as bmi,   b_h/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value/units as bmiUnit,   a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel   from EHR e[ehr_id/value ='{{ehrId}}' ]    contains COMPOSITION a CONTAINS (   OBSERVATION b_c[openEHR-EHR-OBSERVATION.arm_circumference.v0] OR OBSERVATION b_h[openEHR-EHR-OBSERVATION.body_mass_index.v1] OR OBSERVATION b_b[openEHR-EHR-OBSERVATION.body_weight.v2] OR OBSERVATION b_d[openEHR-EHR-OBSERVATION.chest_circumference.v0] OR OBSERVATION b_a[openEHR-EHR-OBSERVATION.height.v1] OR OBSERVATION b_f[openEHR-EHR-OBSERVATION.hip_circumference.v0] OR OBSERVATION b_g[openEHR-EHR-OBSERVATION.thigh_circumference.v0] OR OBSERVATION b_e[openEHR-EHR-OBSERVATION.waist_circumference.v0])"
}
```
