# TemplateId - EHRN Dasha vidha pareeksha.v2
This template is used in Ayurveda examination.
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Dooshyam>Rasa | Boolean | true | |
| 2 | Dooshyam>Raktha | Boolean | true | |
| 3 | Dooshyam>Mamsa | Boolean | true | |
| 4 | Dooshyam>Meda  | Boolean | true | |
| 5 | Dooshyam>Asthi | Boolean | true | |
| 6 | Dooshyam>Majja | Boolean | true | |
| 7 | Dooshyam>Shukra | Boolean | true | |
| 8 | Desham | Text | Jangala | Options - Jangala (at0013); Anoopa (at0014) |
| 9 | Balam | Text | Pravara | Options - Pravara (at0016); Madhyama (at0017); Avara (at0018) |
| 10 | Kalam | Text | Adana | Options - Adana (at0020); Visarga (at0021) |
| 11 | Analam | Text | Tekshna |  Options - Tekshna (at0023); Manda (at0024) |
| 12 | Vayas | Text | Balya | Options - Balya (at0027); Yuva (at0028); Madhyama (at0029); Vridha (at0030) |
| 13 | Satvam | Text | Pravara | Options - Pravara (at0016); Avara (at0018) |
| 14 | Satmyam | Text | Good samyam | |
| 15 | Aharam | Text | Takes nutritious food | |
| 16 | Prakruthi | Text | Prakruthi | |
| 17 | Clinical synopsis | Text | Everything OK | |
## Sample composition
- templateId : EHRN Dasha vidha pareeksha.v2
- format : FLAT

```json
{
"ctx/language": "en",
"dasha_vidha_pareeksha/territory|code": "IN",
"dasha_vidha_pareeksha/territory|terminology": "ISO_3166-1",
"dasha_vidha_pareeksha/language|code": "en",
"dasha_vidha_pareeksha/language|terminology": "ISO_639-1",
"dasha_vidha_pareeksha/composer|id": "1234-5678", 
"dasha_vidha_pareeksha/composer|id_scheme": "UUID", 
"dasha_vidha_pareeksha/composer|id_namespace": "EHR.NETWORK", 
"dasha_vidha_pareeksha/composer|name": "Dileep",
"dasha_vidha_pareeksha/context/health_care_facility|id": "123456-123",
"dasha_vidha_pareeksha/context/health_care_facility|id_scheme": "UUID",
"dasha_vidha_pareeksha/context/health_care_facility|id_namespace": "EHR.NETWORK",
"dasha_vidha_pareeksha/context/health_care_facility|name": "HealtheLife",
"dasha_vidha_pareeksha/context/setting|code": "228",
"dasha_vidha_pareeksha/context/setting|value": "primary medical care",
"dasha_vidha_pareeksha/context/setting|terminology": "openehr",
"dasha_vidha_pareeksha/context/ehrn_metadata:0/confidentiality_level": "5",
"dasha_vidha_pareeksha/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"dasha_vidha_pareeksha/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
"dasha_vidha_pareeksha/context/ehrn_metadata:0/program_details/application_name": "Resort",
"dasha_vidha_pareeksha/context/ehrn_metadata:0/program_details/application_id": "234567-89",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/dooshyam/rasa": true,
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/dooshyam/rakta": true,
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/dooshyam/mamsa": true,
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/dooshyam/meda": true,
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/dooshyam/asthi": true,
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/dooshyam/majja": true,
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/dooshyam/shukra": true,
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/desham|code": "at0013",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/desham|value": "Jangala",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/desham|terminology": "local",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/balam|code": "at0016",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/balam|value": "Pravara",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/balam|terminology": "local",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/kalam|code": "at0020",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/kalam|value": "Adana",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/kalam|terminology": "local",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/analam|code": "at0023",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/analam|value": "Tekshna",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/analam|terminology": "local",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/prakruti/prakruti_type": "DEFAULT_TEXT_VALUE",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/prakruti/analysis_composition": "DEFAULT_TEXT_VALUE",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/vayas|code": "at0027",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/vayas|value": "Balya",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/vayas|terminology": "local",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/satvam|code": "at0016",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/satvam|value": "Pravara",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/satvam|terminology": "local",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/satmyam": "Good samyam",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/aharam": "Takes nutritious food",
"dasha_vidha_pareeksha/dasha_vidha_pareeksha:0/clinical_synopsis": "Everything OK"
}
```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Dasha vidha pareeksha.v2
- personId : 

#### Body : 
```json
{

    "name": "dasha_vidha_pareeksha_aql"
    }

```
or
```json

{
   "name": "dasha_vidha_pareeksha_matches_compositionIDs_aql",
   "query-parameters": {
  "CompositionIDList": "{'830978a7-d08a-4366-ba2a-92a77d0e359d::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
   }
    }
```

### AQL

``` json
{"aql":"selecta/uid/value as uid,a/composer/name as author,a/name/value as template_name,a/context/start_time/value as date_created,b/data[at0001]/events[at0002]/data[at0003]/items[at0004]/items[at0005]/value/value as rasa,b/data[at0001]/events[at0002]/data[at0003]/items[at0004]/items[at0006]/value/value as rakta,b/data[at0001]/events[at0002]/data[at0003]/items[at0004]/items[at0007]/value/value as mamsa,b/data[at0001]/events[at0002]/data[at0003]/items[at0004]/items[at0008]/value/value as meda,b/data[at0001]/events[at0002]/data[at0003]/items[at0004]/items[at0009]/value/value as asthi,b/data[at0001]/events[at0002]/data[at0003]/items[at0004]/items[at0010]/value/value as majja,b/data[at0001]/events[at0002]/data[at0003]/items[at0004]/items[at0011]/value/value as shukra,b/data[at0001]/events[at0002]/data[at0003]/items[at0012]/value/value as desham,b/data[at0001]/events[at0002]/data[at0003]/items[at0012]/value/defining_code/code_string as deshamCode,b/data[at0001]/events[at0002]/data[at0003]/items[at0012]/value/defining_code/terminology_id/value as deshamTerminology,b/data[at0001]/events[at0002]/data[at0003]/items[at0015]/value/value as balam,b/data[at0001]/events[at0002]/data[at0003]/items[at0015]/value/defining_code/code_string as balamCode,b/data[at0001]/events[at0002]/data[at0003]/items[at0015]/value/defining_code/terminology_id/value as balamTerminology,b/data[at0001]/events[at0002]/data[at0003]/items[at0019]/value/value as kalam,b/data[at0001]/events[at0002]/data[at0003]/items[at0019]/value/defining_code/code_string as kalamCode,b/data[at0001]/events[at0002]/data[at0003]/items[at0019]/value/defining_code/terminology_id/value as kalamTerminology,b/data[at0001]/events[at0002]/data[at0003]/items[at0022]/value/value as analam,b/data[at0001]/events[at0002]/data[at0003]/items[at0022]/value/defining_code/code_string as analamCode,b/data[at0001]/events[at0002]/data[at0003]/items[at0022]/value/defining_code/terminology_id/value as analamTerminology,b/data[at0001]/events[at0002]/data[at0003]/items[at0026]/value/value as vayas,b/data[at0001]/events[at0002]/data[at0003]/items[at0026]/value/defining_code/code_string as vayasCode,b/data[at0001]/events[at0002]/data[at0003]/items[at0026]/value/defining_code/terminology_id/value as vayasTerminology,b/data[at0001]/events[at0002]/data[at0003]/items[at0031]/value/value as satvam,b/data[at0001]/events[at0002]/data[at0003]/items[at0031]/value/defining_code/code_string as satvamCode,b/data[at0001]/events[at0002]/data[at0003]/items[at0031]/value/defining_code/terminology_id/value as satvamTerminology,b/data[at0001]/events[at0002]/data[at0003]/items[at0032]/value/value as satmyam,b/data[at0001]/events[at0002]/data[at0003]/items[at0033]/value/value as aharam,b/data[at0001]/events[at0002]/data[at0003]/items[at0036]/items[at0025]/value/value as prakrutiType,b/data[at0001]/events[at0002]/data[at0003]/items[at0034]/value/value as clinicalSynopsis,a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevelFROM EHR e [ehr_id/value = '{{ehrId}}' ]CONTAINS COMPOSITION a CONTAINS EVALUATION b[openEHR-EHR-OBSERVATION.dasha_vidha_pareeksha.v0]"
}
```