# TemplateId - EHRN Aims_objectives.v0
This template is intended to record the person's personal aims and objectives from a clinical encounter(s).
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Aims/objectives | Text | Relax and make lifestyle adjustments. Learn yoga | |

## Sample composition
- templateId : EHRN Aims_objectives.v0
- format : FLAT
```json
{
"ctx/language": "en",
"aims_objectives/territory|code": "IN",
"aims_objectives/territory|terminology": "ISO_3166-1",
"aims_objectives/language|code": "en",
"aims_objectives/language|terminology": "ISO_639-1",
"aims_objectives/composer|id": "1234-5678", 
"aims_objectives/composer|id_scheme": "UUID", 
"aims_objectives/composer|id_namespace": "EHR.NETWORK", 
"aims_objectives/composer|name": "Dileep",
"aims_objectives/context/health_care_facility|id": "123456-123",
"aims_objectives/context/health_care_facility|id_scheme": "UUID",
"aims_objectives/context/health_care_facility|id_namespace": "EHR.NETWORK",
"aims_objectives/context/health_care_facility|name": "HealtheLife",
"aims_objectives/context/setting|code": "232",
"aims_objectives/context/setting|value": "secondary medical care",
"aims_objectives/context/setting|terminology": "openehr",
"aims_objectives/context/ehrn_metadata:0/confidentiality_level": "5",
"aims_objectives/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"aims_objectives/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
"aims_objectives/context/ehrn_metadata:0/program_details/application_name": "Resort",
"aims_objectives/context/ehrn_metadata:0/program_details/application_id": "234567-89",
"aims_objectives/aims_and_objectives:0/aims_objectives": "Relax and make lifestyle adjustments. Learn yoga"
}



```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 


## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Aims_objectives.v0
- personId : 

#### Body : 
```json

{

    "name": "aims_objectives_aql"
    }

```
or
```json

{

    "name": "aims_objectives_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'a502dfb5-b3c4-4411-8d83-cefd850e42a8::local.ethercis.com::1',}"
        }
    }
```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as date_created,b/data[at0001]/items[at0002]/value as aimsObjectives,a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value ='{{ehrId}}' ] CONTAINS COMPOSITION a CONTAINS EVALUATION b[openEHR-EHR-EVALUATION.aims_objectives.v0]"
}
```