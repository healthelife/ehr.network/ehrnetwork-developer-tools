# TemplateId - EHRN Adverse reactions.v0
This composition is intended to be a persistent curated list of a person's allergies & adverse reactions. It has significance across a patient's encounters and is curated from information gathered across clinical events. While it is possible to build a solution to curate this automatically, in realife it is often managed by the care team members.
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Substance | Text | Intolerance to wheat  | Code using SNOMED CT? |
| 2 | Status | coded_text | Suspected | Archetype set - Suspected (at0127); Likely (at0064); Confirmed (at0065); Resolved (at0067); Refuted (at0066) | 
| 3 | Category | coded_text | Food | Archetype set - Food (at0121); Medication (at0122); Other (at0123) |
| 4 | Criticality | coded_text | Low | Archetype set - Low (at0102); High (at0103); Indeterminate (at0124) |
| 5 | Comment | Text | Severe indigestion and bloating | |

## Sample composition
- templateId : EHRN Adverse reactions.v0
- format : FLAT

```json
{
"ctx/language": "en",
"adverse_reaction_list/territory|code": "IN",
"adverse_reaction_list/territory|terminology": "ISO_3166-1",
"adverse_reaction_list/language|code": "en",
"adverse_reaction_list/language|terminology": "ISO_639-1",
"adverse_reaction_list/composer|id": "1234-5678", 
"adverse_reaction_list/composer|id_scheme": "UUID", 
"adverse_reaction_list/composer|id_namespace": "EHR.NETWORK", 
"adverse_reaction_list/composer|name": "Dileep",
"adverse_reaction_list/context/health_care_facility|id": "123456-123",
"adverse_reaction_list/context/health_care_facility|id_scheme": "UUID",
"adverse_reaction_list/context/health_care_facility|id_namespace": "EHR.NETWORK",
"adverse_reaction_list/context/health_care_facility|name": "HealtheLife",
"adverse_reaction_list/context/setting|code": "232",
"adverse_reaction_list/context/setting|value": "secondary medical care",
"adverse_reaction_list/context/setting|terminology": "openehr",
"adverse_reaction_list/context/ehrn_metadata:0/confidentiality_level": "5",
"adverse_reaction_list/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"adverse_reaction_list/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
"adverse_reaction_list/context/ehrn_metadata:0/program_details/application_name": "Clinic",
"adverse_reaction_list/context/ehrn_metadata:0/program_details/application_id": "234567-89",
"adverse_reaction_list/adverse_reaction_risk:0/substance|value": "Intolerance to wheat",
"adverse_reaction_list/adverse_reaction_risk:0/substance|code": "700095006",
"adverse_reaction_list/adverse_reaction_risk:0/substance|terminology": "SNOMED CT",
"adverse_reaction_list/adverse_reaction_risk:0/status|code": "at0127",
"adverse_reaction_list/adverse_reaction_risk:0/status|value": "Suspected",
"adverse_reaction_list/adverse_reaction_risk:0/status|terminology": "local",
"adverse_reaction_list/adverse_reaction_risk:0/category|code": "at0121",
"adverse_reaction_list/adverse_reaction_risk:0/category|value": "Food",
"adverse_reaction_list/adverse_reaction_risk:0/category|terminology": "local",
"adverse_reaction_list/adverse_reaction_risk:0/criticality|code": "at0102",
"adverse_reaction_list/adverse_reaction_risk:0/criticality|value": "Low",
"adverse_reaction_list/adverse_reaction_risk:0/criticality|terminology": "local",
"adverse_reaction_list/adverse_reaction_risk:0/comment": "Severe indigestion and bloating"
}

```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.
#### Parameters
- templateId : EHRN Adverse reactions.v0
- personId : 

#### Body : 
```json
{

    "name": "adverse_reaction_aql"
    }


```
or
```json

{

    "name": "adverse_reaction_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'477a3f31-1e38-4ab8-aeef-1907dfd38558::local.ethercis.com::1'}"
        }
}
```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as createdTime,a/uid/value as uid, b/data[at0001]/items[at0002]/value as substance, b/data[at0001]/items[at0006]/value as description,  b/data[at0001]/items[at0063]/value as status, b/data[at0001]/items[at0101]/value as criticality, b/data[at0001]/items[at0120]/value as category, a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value ='{{ehrId}}' ] CONTAINS COMPOSITION a[openEHR-EHR-COMPOSITION.adverse_reaction_list.v1] CONTAINS EVALUATION b[openEHR-EHR-EVALUATION.adverse_reaction_risk.v1]"
}
```

