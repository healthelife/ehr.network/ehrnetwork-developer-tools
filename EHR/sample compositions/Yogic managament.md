# TemplateId - EHRN Yogic management.v0
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Annamaya kosha | Text | Yogasana | |
| 2 | Pranamaya kosha | Text | Pranayama | | 
| 3 | Manomaya kosha | Text | Meditation | |
| 4 | Vijnanamaya kosha | Text | Meditation | |
| 5 | Anandamaya kosha | Text | Chanting & prayers | |

## Sample composition

- templateId : EHRN Yogic management.v0
- format : FLAT 

```json
{
"ctx/language": "en",
"yogic_management/territory|code": "IN",
"yogic_management/territory|terminology": "ISO_3166-1",
"yogic_management/language|code": "en",
"yogic_management/language|terminology": "ISO_639-1",
"yogic_management/composer|id": "1234-5678", 
"yogic_management/composer|id_scheme": "UUID", 
"yogic_management/composer|id_namespace": "EHR.NETWORK", 
"yogic_management/composer|name": "Dileep",
"yogic_management/context/health_care_facility|id": "123456-123",
"yogic_management/context/health_care_facility|id_scheme": "UUID",
"yogic_management/context/health_care_facility|id_namespace": "EHR.NETWORK",
"yogic_management/context/health_care_facility|name": "HealtheLife",
"yogic_management/context/setting|code": "232",
"yogic_management/context/setting|value": "secondary medical care",
"yogic_management/context/setting|terminology": "openehr",
"yogic_management/context/ehrn_metadata:0/confidentiality_level": "5",
"yogic_management/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"yogic_management/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
"yogic_management/context/ehrn_metadata:0/program_details/application_name": "Resort",
"yogic_management/context/ehrn_metadata:0/program_details/application_id": "234567-89",
"yogic_management/yogic_management:0/current_activity/annamaya_kosha": "Yogasana",
"yogic_management/yogic_management:0/current_activity/pranamaya_kosha": "Pranayama",
"yogic_management/yogic_management:0/current_activity/manomaya_kosha": "Meditation",
"yogic_management/yogic_management:0/current_activity/vijnanamaya_kosha": "Meditation",
"yogic_management/yogic_management:0/current_activity/anandamaya_kosha": "Chanting & prayers",
"yogic_management/yogic_management:0/current_activity/timing": "DEFAULT_TIMING",
"yogic_management/yogic_management:0/narrative": "Recommendations for use of yogic practices"
}



```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Yogic management.v0
- personId : 

#### Body : 
```json
{

    "name": "yogic_management_aql"
    }

```
or
```json

{

    "name": "yogic_management_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    
}
```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as date_created,b/activities[at0001]/description[at0002]/items[at0003]/value as annamayaKosha,b/activities[at0001]/description[at0002]/items[at0004]/value as pranamayaKosha,b/activities[at0001]/description[at0002]/items[at0005]/value as manomayaKosha,b/activities[at0001]/description[at0002]/items[at0006]/value as vijnanamayaKosha,b/activities[at0001]/description[at0002]/items[at0007]/value as anandamayaKosha,a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value ='{{ehrId}}' ] CONTAINS COMPOSITION a CONTAINS INSTRUCTION b[openEHR-EHR-INSTRUCTION.yogic_management.v0]"
}
```
