# TemplateId -  EHRN Procedures.v0
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Procedure name | Text | Procedure on vein  | Code using SNOMED CT? |
| 2 | Final end date/time | Date | | | 
| 3 | Body site | Text | Aortic body | |
| 4 | Description | Text | Description | |
| 5 | Reason | Text | Reason | |

## Sample composition

- templateId : EHRN Procedures.v0
- format : FLAT

```json
{
      "ctx/composer_name": "Dileep",
      "ctx/language": "en",
      "ctx/territory": "IN",
      "procedures/context/start_time": "2020-12-23T12:03:28.134Z",
      "procedures/context/health_care_facility|id": "123456-123",
      "procedures/context/health_care_facility|name": "HealtheLife",
      "procedures/context/health_care_facility|id_scheme": "UUID",
      "procedures/context/health_care_facility|id_namespace": "EHR.NETWORK",
      "procedures/context/setting|code": "228",
      "procedures/context/setting|value": "primary medical care",
      "procedures/context/setting|terminology": "openehr",
      "procedures/context/ehrn_metadata:0/confidentiality_level": "5",
      "procedures/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
      "procedures/context/ehrn_metadata:0/program_details/tenant_id": "ayushehr",
      "procedures/context/ehrn_metadata:0/program_details/application_name": "OP",
      "procedures/context/ehrn_metadata:0/program_details/application_id": "op",
      "procedures/composer|name": "Dileep",
      "procedures/composer|id": "1234-5678",
      "procedures/composer|id_scheme": "UUID",
      "procedures/composer|id_namespace": "EHR.NETWORK",
      "procedures/procedure:0/procedure_name|code": "386609006",
      "procedures/procedure:0/procedure_name|value": "Procedure on vein",
      "procedures/procedure:0/procedure_name|terminology": "SNOMED CT",
      "procedures/procedure:0/description": "Description",
      "procedures/procedure:0/body_site:0|code": "75061007",
      "procedures/procedure:0/body_site:0|value": "Aortic body",
      "procedures/procedure:0/body_site:0|terminology": "SNOMED CT",
      "procedures/procedure:0/final_end_date_time": "2020-12-22T18:30:00.000Z",
      "procedures/procedure:0/reason:0": "Reason",
      "procedures/procedure:0/ism_transition/current_state|value": "planned",
      "procedures/procedure:0/ism_transition/current_state|code": "526",
      "procedures/procedure:0/ism_transition/current_state|terminology": "openehr",
      "procedures/procedure:0/ism_transition/careflow_step|value": "Procedure planned",
      "procedures/procedure:0/ism_transition/careflow_step|code": "at0004",
      "procedures/procedure:0/ism_transition/careflow_step|terminology": "local"
    }





```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Procedures.v0
- personId : 

#### Body : 
```json

{

    "name": "procedures_aql"
    }


```
or
```json

{

    "name": "procedures_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    }

```

### AQL

``` json
{"aql":"select a/composer/name as author,a/uid/value as uid, a/context/start_time/value as createdTime, b/description[at0001]/items[at0002]/value as procedureName,b/description[at0001]/items[at0002]/value/defining_code/code_string as ProblemNameCode, b/description[at0001]/items[at0002]/value/defining_code/terminology_id/value as problemNameTerminology,  b/description[at0001]/items[at0014]/value as reason,  b/description[at0001]/items[at0049]/value as description, b/description[at0001]/items[at0063]/value as bodySite,b/description[at0001]/items[at0063]/value/defining_code/code_string as bodySiteCode, b/description[at0001]/items[at0063]/value/defining_code/terminology_id/value as bodySiteTerminology, a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value ='{{ehrId}}' ] CONTAINS COMPOSITION a CONTAINS ACTION b[openEHR-EHR-ACTION.procedure.v1]"
}
```
