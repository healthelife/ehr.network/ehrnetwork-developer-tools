# TemplateId - EHRN Progress note.v2
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Progress note | Text | Shows good progress | |
| 1 | Synopsis | Text | Continue the treatments | |

## Sample composition

- templateId : EHRN Progress note.v2
- format : FLAT

```json
{
"ctx/language": "en",
"progress_note/territory|code": "IN",
"progress_note/territory|terminology": "ISO_3166-1",
"progress_note/language|code": "en",
"progress_note/language|terminology": "ISO_639-1",
"progress_note/composer|id": "1234-5678", 
"progress_note/composer|id_scheme": "UUID", 
"progress_note/composer|id_namespace": "EHR.NETWORK", 
"progress_note/composer|name": "Dileep",
"progress_note/context/health_care_facility|id": "123456-123",
"progress_note/context/health_care_facility|id_scheme": "UUID",
"progress_note/context/health_care_facility|id_namespace": "EHR.NETWORK",
"progress_note/context/health_care_facility|name": "HealtheLife",
"progress_note/context/setting|code": "232",
"progress_note/context/setting|value": "secondary medical care",
"progress_note/context/setting|terminology": "openehr",
"progress_note/context/ehrn_metadata:0/confidentiality_level": "5",
"progress_note/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"progress_note/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
"progress_note/context/ehrn_metadata:0/program_details/application_name": "Resort",
"progress_note/context/ehrn_metadata:0/program_details/application_id": "234567-89", 
"progress_note/progress_note:0/any_event:0/progress_note": "Shows good progress",
"progress_note/clinical_synopsis:0/synopsis": "Continue the treatments"
}




```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Progress note.v2
- personId : 

#### Body : 
```json

{

    "name": "progress_note_aql"
    }


```
or
```json

{

    "name": "progress_note_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    }
```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as date_created,b/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value as progressNote,c/data[at0001]/items[at0002]/value as actionTaken, a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value ='{{ehrId}}' ] CONTAINS COMPOSITION a CONTAINS (OBSERVATION b[openEHR-EHR-OBSERVATION.progress_note.v0] OR EVALUATION c[openEHR-EHR-EVALUATION.clinical_synopsis.v1])"
}
```
