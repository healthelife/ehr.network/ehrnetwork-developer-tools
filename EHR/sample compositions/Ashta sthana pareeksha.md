# TemplateId - EHRN Ashta sthana pareeksha.v0
This is an Ayurveda examination template. Allows descriptive recording of physician's 8 point observation of the patient. 
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Nadi | Text | Nadi1 | |
| 2 | Mootra | Text | Mootra1 | |
| 3 | Mala | Text | Mala1 | |
| 4 | Jihva | Text | Jihva1 | |
| 5 | Shabda | Text | Shabda1 | |
| 6 | Sparsha | Text | Sparsha1 | |
| 7 | Drik | Text | Drik1 | |
| 8 | Akriti | Text | Akriti1 | |
| 9 | Clinical notes | Text | clinical notes1 | |

## Sample composition
- templateId : EHRN Ashta sthana pareeksha.v0
- format : FLAT

```json
{
"ctx/language": "en",
"ashta_sthana_pariksha/territory|code": "IN",
"ashta_sthana_pariksha/territory|terminology": "ISO_3166-1",
"ashta_sthana_pariksha/language|code": "en",
"ashta_sthana_pariksha/language|terminology": "ISO_639-1",
"ashta_sthana_pariksha/composer|id": "1234-5678", 
"ashta_sthana_pariksha/composer|id_scheme": "UUID", 
"ashta_sthana_pariksha/composer|id_namespace": "EHR.NETWORK", 
"ashta_sthana_pariksha/composer|name": "Dileep",
"ashta_sthana_pariksha/context/health_care_facility|id": "123456-123",
"ashta_sthana_pariksha/context/health_care_facility|id_scheme": "UUID",
"ashta_sthana_pariksha/context/health_care_facility|id_namespace": "EHR.NETWORK",
"ashta_sthana_pariksha/context/health_care_facility|name": "HealtheLife",
"ashta_sthana_pariksha/context/setting|code": "232",
"ashta_sthana_pariksha/context/setting|value": "secondary medical care",
"ashta_sthana_pariksha/context/setting|terminology": "openehr",
"ashta_sthana_pariksha/context/ehrn_metadata:0/confidentiality_level": "5",
"ashta_sthana_pariksha/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"ashta_sthana_pariksha/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
"ashta_sthana_pariksha/context/ehrn_metadata:0/program_details/application_name": "Resort",
"ashta_sthana_pariksha/context/ehrn_metadata:0/program_details/application_id": "234567-89",
"ashta_sthana_pariksha/ashta_sthana_pariksha:0/nadi/description": "Nadi1",
"ashta_sthana_pariksha/ashta_sthana_pariksha:0/mootra/description": "Mootra 1",
"ashta_sthana_pariksha/ashta_sthana_pariksha:0/mala/description": "Mala1",
"ashta_sthana_pariksha/ashta_sthana_pariksha:0/jihva/description": "Jihva1",
"ashta_sthana_pariksha/ashta_sthana_pariksha:0/shabda/decsription": "Shabda 1",
"ashta_sthana_pariksha/ashta_sthana_pariksha:0/sparsha/description": "Sparsha 1",
"ashta_sthana_pariksha/ashta_sthana_pariksha:0/drik/description": "Drik 1",
"ashta_sthana_pariksha/ashta_sthana_pariksha:0/akriti/description": "Akriti 1",
"ashta_sthana_pariksha/ashta_sthana_pariksha:0/clinical_notes": "Clinical notes 1"
}
```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Ashta sthana pareeksha.v0
- personId : 

#### Body : 
```json
{

    "name": "ashta_sthana_pareeksha_aql"
    }


```
or
```json

{
        "name": "ashta_sthana_pareeksha_matches_compositionIDs_aql",
        "query-parameters": {
            "CompositionIDList": "{'63f8d999-3c92-4b84-bd55-72fb9bda6444::local.ethercis.com::1'}"
        }
    }

```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as createdTime, b/data[at0001]/items[at0002]/items[at0003]/value as nadi, b/data[at0001]/items[at0004]/items[at0011]/value as mootra, b/data[at0001]/items[at0005]/items[at0012]/value as mala, b/data[at0001]/items[at0007]/items[at0014]/value as shabda, b/data[at0001]/items[at0006]/items[at0013]/value as jihva, b/data[at0001]/items[at0008]/items[at0015]/value as sparsha, b/data[at0001]/items[at0009]/items[at0016]/value as drik, b/data[at0001]/items[at0010]/items[at0017]/value as akriti, b/data[at0001]/items[at0018]/value as clinicalNotes, a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value ='{{ehrId}}' ] CONTAINS COMPOSITION a[openEHR-EHR-COMPOSITION.encounter.v1] CONTAINS EVALUATION b[openEHR-EHR-EVALUATION.ashta_sthana_pariksha.v0]"
}
```
