# TemplateId - EHRN Exclusions.v0
Records a variety of exclusion statements
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Global exclusion statement name | coded_text | global_exclusion_of_family_history  | Archetype set -  Global exclusion of problems/diagnoses (at0003); global_exclusion_of_family_history (at0004); global_exclusion_of_medication_use (at0005); global_exclusion_of_procedures (at0006); global_exclusion_of_adverse_reactions (at0007) |
| 2 | Global exclusion of problem/diagnoses | text | No hyper tension | | 
## Sample composition
- templateId : EHRN Exclusions.v0
- format : FLAT

```json
{
"ctx/language": "en",
"exclusions/territory|code": "IN",
"exclusions/territory|terminology": "ISO_3166-1",
"exclusions/language|code": "en",
"exclusions/language|terminology": "ISO_639-1",
"exclusions/composer|id": "1234-5678", 
"exclusions/composer|id_scheme": "UUID", 
"exclusions/composer|id_namespace": "EHR.NETWORK", 
"exclusions/composer|name": "Dileep",
"exclusions/context/health_care_facility|id": "123456-123",
"exclusions/context/health_care_facility|id_scheme": "UUID",
"exclusions/context/health_care_facility|id_namespace": "EHR.NETWORK",
"exclusions/context/health_care_facility|name": "HealtheLife",
"exclusions/context/setting|code": "228",
"exclusions/context/setting|value": "primary medical care",
"exclusions/context/setting|terminology": "openehr",
"exclusions/context/ehrn_metadata:0/confidentiality_level": "5",
"exclusions/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"exclusions/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
"exclusions/context/ehrn_metadata:0/program_details/application_name": "Resort",
"exclusions/context/ehrn_metadata:0/program_details/application_id": "234567-89",
"exclusions/exclusion_-_global:0/global_exclusion_of_problems_diagnoses": "No hyper tension"
}
```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId :  EHRN Exclusions.v0
- personId : 

#### Body : 
```json
{

    "name": "exclusions_aql"
    }


```
or
```json
{

    "name": "exclusions_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    }
```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as createdTime, b/data[at0001]/items[at0002]/value as exclusion, b/data[at0001]/items[at0002]/name/value as exclusionName, a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value ='{{ehrId}}' ] CONTAINS COMPOSITION a[openEHR-EHR-COMPOSITION.encounter.v1] CONTAINS EVALUATION b[openEHR-EHR-EVALUATION.exclusion_global.v1]"
}
```
