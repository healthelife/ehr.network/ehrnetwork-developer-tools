# TemplateId - EHRN Food habits.v2
Records variety of food and iet related observations. Used widely by Ayurveda physicians as food management is a very important part of their healthcare practice.
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Diet preferences | Text | Vegan |Options - Vegan (at0043); Lacto-vegetarian (at0044); Lacto-ovo-vegetarian (at0045); Non-vegetarian (at0046); Zero carb (at0047); Gluten free (at0068); Detox (at0069)  |
| 2 | Daily main meal | Text | Breakfast | Options - Breakfast (at0030); Lunch (at0031); Dinner (at0032) |
| 3 | Daily water intake | Text | 1-2 glasses | Options - 1-2 glasses (at0034); 3-4 glasses (at0035); 5-6 glasses (at0036); 7+ glasses (at0037)|
| 4 | Eating behaviour | Text | Eat with full attention | Options - Eat with full attention (at0049); Talk while eating (at0050); Eat quickly (at0051); Watch television (at0052); Eat on-the-go (at0053)|
| 5 | Meal timing | Text | Regular | Options - Regular (at0027); Irregular (at0028)|
| 6 | Taste preference | Text | Sweet | Options - Sweet (at0055); Salty (at0056); Sour (at0057); Bitter (at0058); Hot/spicy (at0059); Starchy (at0060); Oily (at0061) |
| 7 | Taste dislikes | Text | Sour | Options - Sweet (at0055); Salty (at0056); Sour (at0057); Bitter (at0058); Dairy/cheese (at0063); Astrigent (at0064) |
| 8 | Quality of digestion | Text | Fair | Options - Good (at0039); Fair (at0040); Bad (at0041) |
| 9 | Take food between meals | Boolean | true | |
| 10 | Breakfast | Text | Good breakfast | |
| 11 | Lunch | Text | Heavy lunch | |
| 12 | Dinner | Text | Light dinner | |
| 13 | Snacks | Text | Tea & biscuits | |
| 14 | Grains/cereals | Text | Daily | Options - Daily (at0029); Weekly (at0030); Monthly (at0031); Never (at0032) |
| 15 | Vegetables | Text | Daily | Options - Daily (at0029); Weekly (at0030); Monthly (at0031); Never (at0032) |
| 16 | Fruits | Text | Daily | Options - Daily (at0029); Weekly (at0030); Monthly (at0031); Never (at0032) |
| 17 | Dairy products | Text | Daily | Options - Daily (at0029); Weekly (at0030); Monthly (at0031); Never (at0032) |
| 18 | Eggs | Text | Daily | Options - Daily (at0029); Weekly (at0030); Monthly (at0031); Never (at0032) |
| 19 | Poultry | Text | Daily |Options - Daily (at0029); Weekly (at0030); Monthly (at0031); Never (at0032) |
| 20 | Meat | Text | Daily | Options - Daily (at0029); Weekly (at0030); Monthly (at0031); Never (at0032) |
| 21 | Seafood | Text | Daily | Options - Daily (at0029); Weekly (at0030); Monthly (at0031); Never (at0032) |
| 22 | Sugar/honey | Text | Daily | Options - Daily (at0029); Weekly (at0030); Monthly (at0031); Never (at0032) |
| 23 | Sweets | Text | Daily | Options - Daily (at0029); Weekly (at0030); Monthly (at0031); Never (at0032) |
| 24 | Juices | Text | Daily | Options - Daily (at0029); Weekly (at0030); Monthly (at0031); Never (at0032) |

## Sample composition
- templateId : EHRN Food habits.v2
- format : FLAT

```json
{
"ctx/language": "en",
"food_habits/territory|code": "IN",
"food_habits/territory|terminology": "ISO_3166-1",
"food_habits/language|code": "en",
"food_habits/language|terminology": "ISO_639-1",
"food_habits/composer|id": "1234-5678",
"food_habits/composer|id_scheme": "UUID",
"food_habits/composer|id_namespace": "EHR.NETWORK",
"food_habits/composer|name": "Dileep",
"food_habits/context/health_care_facility|id": "123456-123",
"food_habits/context/health_care_facility|id_scheme": "UUID",
"food_habits/context/health_care_facility|id_namespace": "EHR.NETWORK",
"food_habits/context/health_care_facility|name": "HealtheLife",
"food_habits/context/setting|code": "232",
"food_habits/context/setting|value": "secondary medical care",
"food_habits/context/setting|terminology": "openehr",
"food_habits/context/ehrn_metadata:0/confidentiality_level": "5",
"food_habits/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"food_habits/context/ehrn_metadata:0/program_details/tenant_id": "ayushehr",
"food_habits/context/ehrn_metadata:0/program_details/application_name": "Resort",
"food_habits/context/ehrn_metadata:0/program_details/application_id": "Resort",
"food_habits/current_food_habits:0/food_type_frequency:0/grains_cereals/frequency|code": "at0029",  
"food_habits/current_food_habits:0/food_type_frequency:0/grains_cereals/frequency|value": "Daily",  
"food_habits/current_food_habits:0/food_type_frequency:0/grains_cereals/frequency|terminology": "local",
"food_habits/current_food_habits:0/food_type_frequency:0/vegetables/frequency|code": "at0029",  
"food_habits/current_food_habits:0/food_type_frequency:0/vegetables/frequency|value": "Daily",  
"food_habits/current_food_habits:0/food_type_frequency:0/vegetables/frequency|terminology": "local",  
"food_habits/current_food_habits:0/food_type_frequency:0/fruits/frequency|code": "at0029",  
"food_habits/current_food_habits:0/food_type_frequency:0/fruits/frequency|value": "Daily",  
"food_habits/current_food_habits:0/food_type_frequency:0/fruits/frequency|terminology": "local",  
"food_habits/current_food_habits:0/food_type_frequency:0/dairy_products/frequency|code": "at0029",   
"food_habits/current_food_habits:0/food_type_frequency:0/dairy_products/frequency|value": "Daily",   
"food_habits/current_food_habits:0/food_type_frequency:0/dairy_products/frequency|terminology": "local",    
"food_habits/current_food_habits:0/food_type_frequency:0/eggs/frequency|code": "at0029",  
"food_habits/current_food_habits:0/food_type_frequency:0/eggs/frequency|value": "Daily",  
"food_habits/current_food_habits:0/food_type_frequency:0/eggs/frequency|terminology": "local",  
"food_habits/current_food_habits:0/food_type_frequency:0/poultry/frequency|code": "at0029",  
"food_habits/current_food_habits:0/food_type_frequency:0/poultry/frequency|value": "Daily",  
"food_habits/current_food_habits:0/food_type_frequency:0/poultry/frequency|terminology": "local",	 
"food_habits/current_food_habits:0/food_type_frequency:0/meat/frequency|code": "at0029",   
"food_habits/current_food_habits:0/food_type_frequency:0/meat/frequency|value": "Daily",   
"food_habits/current_food_habits:0/food_type_frequency:0/meat/frequency|terminology": "local",	 
"food_habits/current_food_habits:0/food_type_frequency:0/seafood/frequency|code": "at0029",   
"food_habits/current_food_habits:0/food_type_frequency:0/seafood/frequency|value": "Daily",   
"food_habits/current_food_habits:0/food_type_frequency:0/seafood/frequency|terminology": "local",  
"food_habits/current_food_habits:0/food_type_frequency:0/sugar_honey/frequency|code": "at0029",   
"food_habits/current_food_habits:0/food_type_frequency:0/sugar_honey/frequency|value": "Daily",   
"food_habits/current_food_habits:0/food_type_frequency:0/sugar_honey/frequency|terminology": "local",	 
"food_habits/current_food_habits:0/food_type_frequency:0/sweets/frequency|code": "at0029",   
"food_habits/current_food_habits:0/food_type_frequency:0/sweets/frequency|value": "Daily",   
"food_habits/current_food_habits:0/food_type_frequency:0/sweets/frequency|terminology": "local",   
"food_habits/current_food_habits:0/food_type_frequency:0/juices/frequency|code": "at0029",   
"food_habits/current_food_habits:0/food_type_frequency:0/juices/frequency|value": "Daily",   
"food_habits/current_food_habits:0/food_type_frequency:0/juices/frequency|terminology": "local",   
"food_habits/current_food_habits:0/food_habits:0/typical_daily_meal/breakfast": "Good breakfast",   
"food_habits/current_food_habits:0/food_habits:0/typical_daily_meal/lunch": "Heavy lunch",
"food_habits/current_food_habits:0/food_habits:0/typical_daily_meal/dinner": "Light dinner",   
"food_habits/current_food_habits:0/food_habits:0/typical_daily_meal/snacks": "Tea & biscuits",
"food_habits/current_food_habits:0/food_habits:0/diet_preference|code": "at0043",   
"food_habits/current_food_habits:0/food_habits:0/diet_preference|value": "Vegan",   
"food_habits/current_food_habits:0/food_habits:0/diet_preference|terminology": "local",
"food_habits/current_food_habits:0/food_habits:0/daily_main_meal|code": "at0030",   
"food_habits/current_food_habits:0/food_habits:0/daily_main_meal|value": "Breakfast",   
"food_habits/current_food_habits:0/food_habits:0/daily_main_meal|terminology": "local",
"food_habits/current_food_habits:0/food_habits:0/daily_water_intake|code": "at0034",   
"food_habits/current_food_habits:0/food_habits:0/daily_water_intake|value": "1-2 glasses",   
"food_habits/current_food_habits:0/food_habits:0/daily_water_intake|terminology": "local",
"food_habits/current_food_habits:0/food_habits:0/eating_behaviour|code": "at0049",   
"food_habits/current_food_habits:0/food_habits:0/eating_behaviour|value": "Eat with full attention",   
"food_habits/current_food_habits:0/food_habits:0/eating_behaviour|terminology": "local",
"food_habits/current_food_habits:0/food_habits:0/meal_timing|code": "at0027",   
"food_habits/current_food_habits:0/food_habits:0/meal_timing|value": "Regular",   
"food_habits/current_food_habits:0/food_habits:0/meal_timing|terminology": "local",   
"food_habits/current_food_habits:0/food_habits:0/take_food_between_meals": true,   
"food_habits/current_food_habits:0/food_habits:0/taste_preferences:0|code": "at0055",   
"food_habits/current_food_habits:0/food_habits:0/taste_preferences:0|value": "Sweet",   
"food_habits/current_food_habits:0/food_habits:0/taste_preferences:0|terminology": "local",
"food_habits/current_food_habits:0/food_habits:0/taste_dislikes:0|code": "at0055",   
"food_habits/current_food_habits:0/food_habits:0/taste_dislikes:0|value": "Sweet",   
"food_habits/current_food_habits:0/food_habits:0/taste_dislikes:0|terminology": "local",
"food_habits/current_food_habits:0/food_habits:0/quality_of_digestion|code": "at0039",   
"food_habits/current_food_habits:0/food_habits:0/quality_of_digestion|value": "Good",   
"food_habits/current_food_habits:0/food_habits:0/quality_of_digestion|terminology": "local"
}
```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 

## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId : EHRN Food habits.v2
- personId : 

#### Body : 
```json

{

    "name": "food_habits_aql"
    }


```
or
```json

{

    "name": "food_habits_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
    }
```

### AQL

``` json
{"aql":"select a/composer/name as author,a/context/start_time/value as created_time,b/data[at0001]/events[at0002]/data[at0003]/items[at0021]/items[at0022]/value as breakfast,b/data[at0001]/events[at0002]/data[at0003]/items[at0021]/items[at0023]/value as lunch,b/data[at0001]/events[at0002]/data[at0003]/items[at0021]/items[at0024]/value as dinner,b/data[at0001]/events[at0002]/data[at0003]/items[at0021]/items[at0025]/value as snacks,b/data[at0001]/events[at0002]/data[at0003]/items[at0026]/value as mealTiming,b/data[at0001]/events[at0002]/data[at0003]/items[at0026]/value/defining_code/code_string as mealTimingCode,b/data[at0001]/events[at0002]/data[at0003]/items[at0026]/value/defining_code/terminology_id/value as mealTimingTerminology,b/data[at0001]/events[at0002]/data[at0003]/items[at0029]/value as dailyMainMeal,b/data[at0001]/events[at0002]/data[at0003]/items[at0029]/value/defining_code/code_string as dailyMainMealCode,b/data[at0001]/events[at0002]/data[at0003]/items[at0029]/value/defining_code/terminology_id/value as dailyMainMealTerminology,b/data[at0001]/events[at0002]/data[at0003]/items[at0033]/value as dailyWaterIntake,b/data[at0001]/events[at0002]/data[at0003]/items[at0033]/value/defining_code/code_string as dailyWaterIntakeCode,b/data[at0001]/events[at0002]/data[at0003]/items[at0033]/value/defining_code/terminology_id/value as dailyWaterIntakeTerminology,b/data[at0001]/events[at0002]/data[at0003]/items[at0038]/value as digestion,b/data[at0001]/events[at0002]/data[at0003]/items[at0038]/value/defining_code/code_string as digestionCode,b/data[at0001]/events[at0002]/data[at0003]/items[at0038]/value/defining_code/terminology_id/value as digestionTerminology,b/data[at0001]/events[at0002]/data[at0003]/items[at0042]/value as dietPreference,b/data[at0001]/events[at0002]/data[at0003]/items[at0042]/value/defining_code/code_string as dietPreferenceCode,b/data[at0001]/events[at0002]/data[at0003]/items[at0042]/value/defining_code/terminology_id/value as dietPreferenceTerminology,b/data[at0001]/events[at0002]/data[at0003]/items[at0048]/value as eatingBehaviour,b/data[at0001]/events[at0002]/data[at0003]/items[at0048]/value/defining_code/code_string as eatingBehaviourCode,b/data[at0001]/events[at0002]/data[at0003]/items[at0048]/value/defining_code/terminology_id/value as eatingBehaviourTerminology,b/data[at0001]/events[at0002]/data[at0003]/items[at0062]/value as tasteDislikes,b/data[at0001]/events[at0002]/data[at0003]/items[at0062]/value/defining_code/code_string as tasteDislikesCode,b/data[at0001]/events[at0002]/data[at0003]/items[at0062]/value/defining_code/terminology_id/value as tasteDislikesTerminology,b/data[at0001]/events[at0002]/data[at0003]/items[at0054]/value as tastePreferences,b/data[at0001]/events[at0002]/data[at0003]/items[at0054]/value/defining_code/code_string as tastePreferencesCode,b/data[at0001]/events[at0002]/data[at0003]/items[at0054]/value/defining_code/terminology_id/value as tastePreferencesTerminology,b/data[at0001]/events[at0002]/data[at0003]/items[at0065]/value as takeFoodBetweenMeals,c/data[at0001]/events[at0002]/data[at0003]/items[at0024]/items[at0028]/value as grains,c/data[at0001]/events[at0002]/data[at0003]/items[at0024]/items[at0028]/value/defining_code/code_string as grainsCode,c/data[at0001]/events[at0002]/data[at0003]/items[at0024]/items[at0028]/value/defining_code/terminology_id/value as grainsTerminology,c/data[at0001]/events[at0002]/data[at0003]/items[at0033]/items[at0043]/value as veggies,c/data[at0001]/events[at0002]/data[at0003]/items[at0033]/items[at0043]/value/defining_code/code_string as veggiesCode,c/data[at0001]/events[at0002]/data[at0003]/items[at0033]/items[at0043]/value/defining_code/terminology_id/value as veggiesTerminology,c/data[at0001]/events[at0002]/data[at0003]/items[at0034]/items[at0045]/value as fruits,c/data[at0001]/events[at0002]/data[at0003]/items[at0034]/items[at0045]/value/defining_code/code_string as fruitsCode,c/data[at0001]/events[at0002]/data[at0003]/items[at0034]/items[at0045]/value/defining_code/terminology_id/value as fruitsTerminology,c/data[at0001]/events[at0002]/data[at0003]/items[at0035]/items[at0046]/value as dairy,c/data[at0001]/events[at0002]/data[at0003]/items[at0035]/items[at0046]/value/defining_code/code_string as dairyCode,c/data[at0001]/events[at0002]/data[at0003]/items[at0035]/items[at0046]/value/defining_code/terminology_id/value as dairyTerminology,c/data[at0001]/events[at0002]/data[at0003]/items[at0036]/items[at0047]/value as eggs,c/data[at0001]/events[at0002]/data[at0003]/items[at0036]/items[at0047]/value/defining_code/code_string as eggsCode,c/data[at0001]/events[at0002]/data[at0003]/items[at0036]/items[at0047]/value/defining_code/terminology_id/value as eggsTerminology,c/data[at0001]/events[at0002]/data[at0003]/items[at0037]/items[at0048]/value as poultry,c/data[at0001]/events[at0002]/data[at0003]/items[at0037]/items[at0048]/value/defining_code/code_string as poultryCode,c/data[at0001]/events[at0002]/data[at0003]/items[at0037]/items[at0048]/value/defining_code/terminology_id/value as poultryTerminology,c/data[at0001]/events[at0002]/data[at0003]/items[at0038]/items[at0049]/value as meat,c/data[at0001]/events[at0002]/data[at0003]/items[at0038]/items[at0049]/value/defining_code/code_string as meatCode,c/data[at0001]/events[at0002]/data[at0003]/items[at0038]/items[at0049]/value/defining_code/terminology_id/value as meatTerminology,c/data[at0001]/events[at0002]/data[at0003]/items[at0039]/items[at0050]/value as seafood,c/data[at0001]/events[at0002]/data[at0003]/items[at0039]/items[at0050]/value/defining_code/code_string as seafoodCode,c/data[at0001]/events[at0002]/data[at0003]/items[at0039]/items[at0050]/value/defining_code/terminology_id/value as seafoodTerminology,c/data[at0001]/events[at0002]/data[at0003]/items[at0040]/items[at0051]/value as sugarHoney,c/data[at0001]/events[at0002]/data[at0003]/items[at0040]/items[at0051]/value/defining_code/code_string as sugarHoneyCode,c/data[at0001]/events[at0002]/data[at0003]/items[at0040]/items[at0051]/value/defining_code/terminology_id/value as sugarHoneyTerminology,c/data[at0001]/events[at0002]/data[at0003]/items[at0041]/items[at0052]/value as sweets,c/data[at0001]/events[at0002]/data[at0003]/items[at0041]/items[at0052]/value/defining_code/code_string as sweetsCode,c/data[at0001]/events[at0002]/data[at0003]/items[at0041]/items[at0052]/value/defining_code/terminology_id/value as sweetsTerminology,c/data[at0001]/events[at0002]/data[at0003]/items[at0042]/items[at0053]/value as juices,c/data[at0001]/events[at0002]/data[at0003]/items[at0042]/items[at0053]/value/defining_code/code_string as juicesCode,c/data[at0001]/events[at0002]/data[at0003]/items[at0042]/items[at0053]/value/defining_code/terminology_id/value as juicesTerminology,a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value ='{{ehrId}}' ] CONTAINS COMPOSITION a CONTAINS (OBSERVATION b[openEHR-EHR-OBSERVATION.food_habits.v0] or OBSERVATION c[openEHR-EHR-OBSERVATION.food_type_frequency.v0])"
}
```
