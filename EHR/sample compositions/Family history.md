# TemplateId - EHRN Family history.v1
Records the history of significant health related issues in related family members of the patient. It has significance across a patient's encounters.
## Data points 
The following data points that are included in the sample composition below. If you need to support any addtional data point in your application, please discuss with our support team.
| # | Item | Data type | Sample | Additional details |
|---|-----|-----------|---------|---------|
| 1 | Relationship | Text | Father  | Code using SNOMED CT? |
| 2 | Problem/Diagnosis name | Text | Diabetes mellitus | Code using SNOMED CT? | 
| 3 | Clinical description | Text | Description of family history | |
| 4 | Age at onset | Duration | 52 Years | PT0S format |

## Sample composition
- templateId : EHRN Family history.v1
- format : FLAT

```json
{
"ctx/language": "en",
"family_history/territory|code": "IN",
"family_history/territory|terminology": "ISO_3166-1",
"family_history/language|code": "en",
"family_history/language|terminology": "ISO_639-1",
"family_history/composer|id": "1234-5678", 
"family_history/composer|id_scheme": "UUID", 
"family_history/composer|id_namespace": "EHR.NETWORK", 
"family_history/composer|name": "Dileep",
"family_history/context/health_care_facility|id": "123456-123",
"family_history/context/health_care_facility|id_scheme": "UUID",
"family_history/context/health_care_facility|id_namespace": "EHR.NETWORK",
"family_history/context/health_care_facility|name": "HealtheLife",
"family_history/context/setting|code": "232",
"family_history/context/setting|value": "secondary medical care",
"family_history/context/setting|terminology": "openehr",
"family_history/context/ehrn_metadata:0/confidentiality_level": "5",
"family_history/context/ehrn_metadata:0/program_details/tenant_name": "AyushEHR",
"family_history/context/ehrn_metadata:0/program_details/tenant_id": "234567-89",
"family_history/context/ehrn_metadata:0/program_details/application_name": "Resort",
"family_history/context/ehrn_metadata:0/program_details/application_id": "234567-89",
"family_history/family_history:0/per_family_member:0/relationship|value": "Father",
"family_history/family_history:0/per_family_member:0/relationship|code": "66839005",
"family_history/family_history:0/per_family_member:0/relationship|terminology": "SNOMED CT",
"family_history/family_history:0/per_family_member:0/clinical_history:0/problem_diagnosis_name|value": "Diabetes mellitus",
"family_history/family_history:0/per_family_member:0/clinical_history:0/problem_diagnosis_name|code": "73211009",
"family_history/family_history:0/per_family_member:0/clinical_history:0/problem_diagnosis_name|terminology": "SNOMED CT", 
"family_history/family_history:0/per_family_member:0/clinical_history:0/clinical_description": "Description of family history",  
"family_history/family_history:0/per_family_member:0/clinical_history:0/age_at_onset": "P52YT0S"
}
```
### Things to note
- All dates are in ISO format
- For the appropriate **setting** code & value, refer to [openEHR terminology](https://specifications.openehr.org/releases/TERM/latest)
- Use appropriate details of **composer** and **healthcare facility** depending upon the login of the user creating the composition
- You can add multiple sets of any data using this template. Please discuss with our support team to know more 


## Queries
The data committed using the above composition can be retrieved using either the stored queries or direct AQLs. The SAAS platform allows only stored queries. However if you are running our BOX option, you can directly use AQLs on the query interface. 

### Stored query
Stored queries are the pre-defined queries used to retrieve the data by suppling qualified_query_name i.e. the name of the query to be executed and passing query_parameter. The format is given below in an example.

#### Parameters
- templateId :  EHRN Family history.v1
- personId : 

#### Body : 
```json


{

    "name": "family_history_aql"
    }


```
or
```json

{

    "name": "family_history_matches_compositionIDs_aql",
    "query-parameters": 

        {
            "CompositionIDList": "{'f1d9e57c-19ea-431e-b1de-0bcb86bb3e79::local.ethercis.com::1','c251a3a6-ae43-4e17-9a72-6fd9a1df5dfd::local.ethercis.com::1'}"
        }
}

```

### AQL

``` json
{"aql":"select a/composer/name as author, a/context/start_time/value as createdTime, b/data[at0001]/items[at0003]/items[at0016]/value as relatonship, b/data[at0001]/items[at0003]/items[at0016]/value/defining_code/code_string as relationshipCode, b/data[at0001]/items[at0003]/items[at0016]/value/defining_code/terminology_id/value as relationshipCodeTerminology, b/data[at0001]/items[at0003]/items[at0008]/items[at0009]/value as problemName, b/data[at0001]/items[at0003]/items[at0008]/items[at0009]/value/defining_code/code_string as problemNameCode, b/data[at0001]/items[at0003]/items[at0008]/items[at0009]/value/defining_code/terminology_id/value as problemNameCodeTerminology, b/data[at0001]/items[at0003]/items[at0008]/items[at0012]/value as clinicalDescription, b/data[at0001]/items[at0003]/items[at0008]/items[at0010]/value as ageAtOnset, a/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.ehrn_metadata.v0]/items[at0001]/value/magnitude as confidentialLevel FROM EHR e[ehr_id/value ='{{ehrId}}' ] CONTAINS COMPOSITION a[openEHR-EHR-COMPOSITION.review.v1] CONTAINS EVALUATION b[openEHR-EHR-EVALUATION.family_history.v2]"
}
```