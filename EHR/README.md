# Resources on EHR.Network CDR for developers
We will be adding documentation and resources related to EHR.Network openEHR based CDR in this folder. Out attempt is to make things easy for application developers to adopt openEHR in general and EHR.Network in particular to build modern, future prooof clinical applications.

The resources available here will flatten your learning curve and get you going with minimal effort so that you can focus on delivering cutting edge solutions to your customers fast. Some of the resources that it contains currently  include
- List of default templates available on the platform
- All the templates deployed on the platform. These are provided in openEHR template(OPT) format freely for any one to take and use
- More details on using the above templates - Example data points, sample composition format, stored queries and AQLs. All these can be used along with the templates on any openEHR compliant repository
- Postman collections 

The resources available here are best used along with the rest of our [platform documentation](https://docs.ehr.network/site/api_documentation/) 