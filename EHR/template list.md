# Templates available in EHR.Network 
## General
### Medical history
- EHRN Problem list.v1
- EHRN Adverse reactions.v0
- EHRN Medication summary.v0
- EHRN Family history.v1
- EHRN Activity summary.v1
- EHRN Substance use.v1
- EHRN Tobacco smoking summary.v0
- EHRN Alcohol consumption summary.v0
- EHRN Menstrual summary.v2
- EHRN Exclusions.v0
### Consult
- EHRN Complaints.v2
- EHRN Vital signs.v1
- EHRN Anthropometry.v2
- EHRN Systemic examination.v1
- EHRN Clinical notes.v1
- EHRN Lab test results.v0
- EHRN Imaging examinations result.v0
- EHRN Diagnosis.v1
- EHRN Procedures.v0
### Care plan
- EHRN Aims_objectives.v0
- EHRN Goals.v0
- EHRN Medication order.v2
- EHRN Service order.v1
- EHRN Lifestyle advises.v0
- EHRN Progress note.v2
## Ayurveda & wellness
- EHRN Ashta sthana pareeksha.v0
- EHRN Dasha vidha pareeksha.v2
- EHRN General examination.v1
- EHRN Food habits.v2
- EHRN Food preference.v0
- EHRN Samprapti ghataka.v0
- EHRN Wellness summary.v2
- EHRN Yogic management.v0
