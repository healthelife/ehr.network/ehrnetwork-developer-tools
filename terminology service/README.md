# SNOMED CT look up service 
This service can be used to intergrate SNOMED CT search and lookups in your application. Provides REST APIs that can be used by your application to lookup clinical terminologies and codes from SNOMED CT and use them to code your EHR data. 
Coding of clinical data makes them standards compliant and interoperable as per the Indian EHR standards. So if you are building a new EHR application or are planing to make your EHR standards compliant, this service will come handy for you.

## Important license details to use the service
This service is intended for applications used inside India. Before you use this service please visit [this page](https://mlds.ihtsdotools.org/#/landing/IN?lang=en) and get a free associate license for using SNOMED CT.

## More details and documentation
- [EHR.Network Terminology service – SNOMED CT, ICD look up & drug database APIs](https://ehr.network/ehr-as-a-platform-for-healthcare-application-development/collection-of-services-orchestration-of-services-to-delivers-cloud-native-experience/ehr-network-terminology-service-snomed-ct-icd-look-up-drug-database-apis/)

## Demo instance 
The link below allow you to try out the service and APIs on your browser. The Postman collection folder contains the server environment and an API set collection for the demo. This demo APIs have been load limited to prevent misuse and so cannot be used for production. 
- [SNOMED CT server APIs](https://ehr.network/ehr-as-a-platform-for-healthcare-application-development/collection-of-services-orchestration-of-services-to-delivers-cloud-native-experience/ehr-network-terminology-service-sample-apis-for-demo/)
 

## Production instance
The production service requires an API key. You will get this when you sign up for using this service. Please visit [EHR.Network website](https://ehr.network) to know more.

## Search parameters
The terminology service APIs accept many parameters to fine tune your search results. If these are used appropriately, you can get the appropriate results in your searches. Please see below a list of suggested parameters for some of the important concepts used commonly in EHRs.

| Concept | API | state | semantictag | acceptability | returnlimit | groupbyconcept |
| ------- | --- | ----- | ----------- | ------------- | ----------- | -------------- |
| Relationship | Suggest/search | active | person | synonyms | 50 | true |
| Nationality | Suggest/search | active | geographic location | synonyms | 50 | true |
| Complaint | Suggest/search | active | finding | synonyms | 50 | true |
| Problem/diagnosis | Suggest/search | active | disorder | synonyms | 50 | true |
| Body site | Suggest/search | active | body structure | synonyms | 50 | true |
| Med - item | Suggest/search | active | clinical drug | synonyms | 50 | true |
| Med - Form & route | Suggest/search | active | dose form | synonyms | 50 | true |
| Med - Dose(unit) | Suggest/search | active | qualifier value | synonyms | 50 | true |
| Med - Route | Suggest/search | active | qualifier value | synonyms | 50 | true |
| Med - Frequencies | Suggest/search | active | qualifier value | synonyms | 50 | true |
| Site of admin | Suggest/search | active | body structure | synonyms | 50 | true |
| Allergy | Suggest/search | active | finding | synonyms | 50 | true |
| Allergy - substance | Suggest/search | active | substance | synonyms | 50 | true |
| Allergy - reaction | Suggest/search | active | finding | synonyms | 50 | true |
| Procedure | Suggest/search | active | procedure | synonyms | 50 | true |
| System of structure | Suggest/search | active | body structure | synonyms | 50 | true |
| Lab - order | Suggest/search | active | procedure | synonyms | 50 | true |
| Lab - test | Suggest/search | active | procedure | synonyms | 50 | true |
| Lab - analyte | Suggest/search | active | specimen | synonyms | 50 | true |
| Lab  - result unit | Suggest/search | active | qualifier value | synonyms | 50 | true |
| Imaging - order | Suggest/search | active | procedure | synonyms | 50 | true |
| Imaging - exam | Suggest/search | active | procedure | synonyms | 50 | true |

## Understanding more about the parameters used in suggest/search
- Semantic tag - https://confluence.ihtsdotools.org/display/DOCEG/Semantic+Tags
