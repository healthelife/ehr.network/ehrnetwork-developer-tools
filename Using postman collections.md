The postman collections that you find in this repository can be imported into your postman application and used to test the EHR.Network APIs.

Details on how to use the API can be found in the descriptions section. To view the description, click on the accordion on the left

Many of the collections include multiple examples for each of the APIs that the platform supports. The list oif available options can be accessed by clicking on the examples link to the right of the screen. You can select the examples by clicking on the items displayed in the dropdown.